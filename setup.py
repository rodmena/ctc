import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "ctc",
    version = read('VERSION').replace('\n',''),
    author = "Farsheed Ashouri",
    author_email = "farsheed.ashouri@gmail.com",
    description = ("Fastest Cache Server"),
    license = "copyrighted by Chista Co.",
    keywords = "cache, geoserver, tilecache, proxy",
    url = "http://chista.ir/dev/ctc",
    packages=['ctc', 'ctc/packages'],
    include_package_data=False,
    #long_description=read('Readme.MD'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: Chista Co.",
    ],
)
