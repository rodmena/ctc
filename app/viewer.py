
import os
import time
from flask import Flask
from flask import render_template
import ConfigParser
import ctc.utils as utils

app = Flask(__name__)
app.debug = True

#@utils.Memoized
def find_layers():
    '''Find layers in config'''
    ctc_config = ConfigParser.RawConfigParser()
    ctc_config.read('config/ctc.ini')
    sections = ctc_config.sections()
    #sections.reverse()
    if 'global_settings' in sections:
        sections.remove('global_settings')
    return sections

@utils.Memoized
def get_ip_and_port():
    '''Find correct ip from ctc.conf in nginx settings'''
    ngc = open('bin/nginx/ctc.conf', 'r').readlines()
    data = [line.strip().replace(';', '') for line in ngc if 'listen' in line]
    port = int(data[0].split()[1])
    data = [line.strip().replace(';', '') for line in ngc if 'server_name' in line]
    ip = data[0].split()[1]
    return ip, port


@app.route('/')
def index():
    ''' Well, This is a nice start '''
    ip, port = get_ip_and_port()
    layers = find_layers()
    return render_template('index.html', 
                           layers=layers,
                           ip=ip,
                           port=port)
                            


