function convert2MinMax(val,min,max)
{
    var ret = val;
    
    if (val < min)
    {
        ret = min;
    }
    else if (val > max)
    {
        ret = max;
    }
    return ret;
}
function lineIntersect(x0,y0,x1,y1,x2,y2,x3,y3)
{    
    if ((x1-x0)!=0)
    {
        var m1 = (y1-y0)/(x1-x0);
    }
    else
    {
        var m1 = parseFloat(1000000000000000);   // close enough to infinity
    }

    if ((x3-x2)!=0)
    {
        var m2 = (y3-y2)/(x3-x2);
    }
    else
    {
       var m2 = parseFloat(1000000000000000); // close enough to infinity
    }

    var b1 = -1;
    var b2 = -1;

    var c1 = (y0-m1*x0);
    var c2 = (y2-m2*x2);

    var det_inv = 1/(m1*b2 - m2*b1);

    var xi = ((b1*c2 - b2*c1)*det_inv);
    var yi = ((m2*c1 - m1*c2)*det_inv);    
  
    var lineIntersectArr=new Array();
		lineIntersectArr[0]=xi;
		lineIntersectArr[1]=yi;	
	return lineIntersectArr;
}
function cutLine2Box(lineShape,lineArrX,lineArrY,boxTLX,boxTLY,boxBRX,boxBRY)
{
    var tempXArr = new Array();
    var tempYArr = new Array();
   
    /*
    window.alert(""+
    " \n  lineShape :  " +lineShape + 
    " \n  lineArrX.length :  " +lineArrX.length + 
    " \n  tempRunLen :  " +tempRunLen + 
    "");
    */
    
    var tGeomStatD = "out";    
    
    var tGeomLastCor = "no";
    var tGeomLastStat = "no";
    var tGeomLastSid = "no";
    
    for (var k = 0; k < lineArrX.length-1; k++)  
    {
        var tempRunLenStat;

        var tGeomPntX1 = lineArrX[k];
        var tGeomPntY1 = lineArrY[k];
        var tGeomPntX2 = lineArrX[k+1];
        var tGeomPntY2 = lineArrY[k+1];
               
        var tGeomStatP1 = "in";                
        var tGeomStatP2 = "in";
        var tGeomStatB = "in";
        
        if ((tGeomPntX1 < boxTLX) || (tGeomPntX1 > boxBRX) || (tGeomPntY1 < boxTLY) || (tGeomPntY1 > boxBRY))
        {
            tGeomStatP1 = "out";
        }
        if ((tGeomPntX2 < boxTLX) || (tGeomPntX2 > boxBRX) || (tGeomPntY2 < boxTLY) || (tGeomPntY2 > boxBRY))
        {
            tGeomStatP2 = "out";
        }
        
        if ((tGeomStatP1 == "out") && (tGeomStatP2 == "out"))
        {
            var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxTLX,boxTLY,boxBRX,boxTLY);
            var tBoxTopX = Math.round(tLineInterArray[0]);
            var tBoxTopY = Math.round(tLineInterArray[1]);

            var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxTLX,boxBRY,boxBRX,boxBRY);
            var tBoxBotX = Math.round(tLineInterArray[0]);
            var tBoxBotY = Math.round(tLineInterArray[1]);

            var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxTLX,boxTLY,boxTLX,boxBRY);
            var tBoxLeftX = Math.round(tLineInterArray[0]);
            var tBoxLeftY = Math.round(tLineInterArray[1]);

            var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxBRX,boxTLY,boxBRX,boxBRY);
            var tBoxRightX = Math.round(tLineInterArray[0]);
            var tBoxRightY = Math.round(tLineInterArray[1]);
           
            if (((tBoxTopX >= boxTLX) && (tBoxTopX <= boxBRX)) && (((tBoxTopX <= tGeomPntX1) && (tBoxTopX >= tGeomPntX2)) || ((tBoxTopX <= tGeomPntX2) && (tBoxTopX >= tGeomPntX1))))
            {
                tGeomStatB = "in";
            }
            else if (((tBoxBotX >= boxTLX) && (tBoxTopX <= boxBRX)) && (((tBoxBotX <= tGeomPntX1) && (tBoxBotX >= tGeomPntX2)) || ((tBoxBotX <= tGeomPntX2) && (tBoxBotX >= tGeomPntX1))))
            {
                tGeomStatB = "in";
            }
            else if (((tBoxLeftY >= boxTLY) && (tBoxLeftY <= boxBRY)) && (((tBoxLeftY <= tGeomPntY1) && (tBoxLeftY >= tGeomPntY2)) || ((tBoxLeftY <= tGeomPntY2) && (tBoxLeftY >= tGeomPntY1))))
            {
                tGeomStatB = "in";
            }
            else if (((tBoxRightY >= boxTLY) && (tBoxRightY <= boxBRY)) && (((tBoxRightY <= tGeomPntY1) && (tBoxRightY >= tGeomPntY2)) || ((tBoxRightY <= tGeomPntY2) && (tBoxRightY >= tGeomPntY1))))
            {
                tGeomStatB = "in";
            }
            else
            {
                tGeomStatB = "out";
            }
        }
        
        if (k == 0)
        {                    
            //tempXArr[tempXArr.length] = convert2MinMax(parseInt(tGeomPntX1),boxTLX,boxBRX);
            //tempYArr[tempYArr.length] = convert2MinMax(parseInt(tGeomPntY1),boxTLY,boxBRY);
        }
        
        if (tGeomStatB == "out")
        {
            tGeomLastStat = "out";
            
            if ((tBoxTopX < boxTLX) && (((tBoxTopY <= tGeomPntY1) && (tBoxTopY >= tGeomPntY2)) || ((tBoxTopY <= tGeomPntY2) && (tBoxTopY >= tGeomPntY1))))
            {
                tempXArr[tempXArr.length] = boxTLX; //TL
                tempYArr[tempYArr.length] = boxTLY;
            }
            if ((tBoxLeftY < boxTLY) && (((tBoxLeftX <= tGeomPntX1) && (tBoxLeftX >= tGeomPntX2)) || ((tBoxLeftX <= tGeomPntX2) && (tBoxLeftX >= tGeomPntX1))))
            {                
                tempXArr[tempXArr.length] = boxTLX; //TL
                tempYArr[tempYArr.length] = boxTLY;
            }
            if ((tBoxBotX < boxTLX) && (((tBoxBotY <= tGeomPntY1) && (tBoxBotY >= tGeomPntY2)) || ((tBoxBotY <= tGeomPntY2) && (tBoxBotY >= tGeomPntY1))))
            {                
                tempXArr[tempXArr.length] = boxTLX; //BL
                tempYArr[tempYArr.length] = boxBRY;
            }
            if ((tBoxLeftY > boxBRY) && (((tBoxLeftX <= tGeomPntX1) && (tBoxLeftX >= tGeomPntX2)) || ((tBoxLeftX <= tGeomPntX2) && (tBoxLeftX >= tGeomPntX1))))
            {                
                tempXArr[tempXArr.length] = boxTLX; //BL
                tempYArr[tempYArr.length] = boxBRY;
            }
            if ((tBoxTopX > boxBRX) && (((tBoxTopY <= tGeomPntY1) && (tBoxTopY >= tGeomPntY2)) || ((tBoxTopY <= tGeomPntY2) && (tBoxTopY >= tGeomPntY1))))
            {                
                tempXArr[tempXArr.length] = boxBRX; //TR
                tempYArr[tempYArr.length] = boxTLY;
            }
            if ((tBoxRightY < boxTLY) && (((tBoxRightX <= tGeomPntX1) && (tBoxRightX >= tGeomPntX2)) || ((tBoxRightX <= tGeomPntX2) && (tBoxRightX >= tGeomPntX1))))
            {                
                tempXArr[tempXArr.length] = boxBRX; //TR
                tempYArr[tempYArr.length] = boxTLY;
            }
            if ((tBoxBotX > boxBRX) && (((tBoxBotY <= tGeomPntY1) && (tBoxBotY >= tGeomPntY2)) || ((tBoxBotY <= tGeomPntY2) && (tBoxBotY >= tGeomPntY1))))
            {                
                tempXArr[tempXArr.length] = boxBRX; //BR
                tempYArr[tempYArr.length] = boxBRY;
            }
            if ((tBoxRightY > boxBRY) && (((tBoxRightX <= tGeomPntX1) && (tBoxRightX >= tGeomPntX2)) || ((tBoxRightX <= tGeomPntX2) && (tBoxRightX >= tGeomPntX1))))
            {                
                tempXArr[tempXArr.length] = boxBRX; //BR
                tempYArr[tempYArr.length] = boxBRY;
            }
        }
        else if ((tGeomStatP1 == "out") || (tGeomStatP2 == "out"))
        {
            if (tGeomStatP1 == "in")
            {
                tempXArr[tempXArr.length] = parseInt(tGeomPntX1);
                tempYArr[tempYArr.length] = parseInt(tGeomPntY1);
            }
            else if (tGeomStatP1 == "out")
            {
                var tempXValX = "NA";
                var tempXValY = "NA";
                var tempYValX = "NA";
                var tempYValY = "NA";
                
                var tempValSid = "NA";
                
                if (tGeomPntX1 < boxTLX) 
                {                    
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxTLX,parseFloat(10000000000000),boxTLX,parseFloat(-10000000000000));
                    
                    tempXValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempXValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntX1 > tempXValX)
                    {
                        tempXValX = "NA";
                        tempXValY = "NA";
                    }
                    else
                    {
                        tempValSid = "left";
                    }
                }
                else if (tGeomPntX1 > boxBRX) 
                {
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxBRX,parseFloat(10000000000000),boxBRX,parseFloat(-10000000000000));
                    
                    tempXValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempXValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntX1 < tempXValX)
                    {
                        tempXValX = "NA";
                        tempXValY = "NA";
                    }
                    else
                    {
                        tempValSid = "right";
                    }
                }  
                if (tGeomPntY1 < boxTLY) 
                {
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,parseFloat(10000000000000),boxTLY,parseFloat(-10000000000000),boxTLY);
                    
                    tempYValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempYValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntY1 > tempYValY)
                    {
                        tempYValX = "NA";
                        tempYValY = "NA";
                    }
                    else
                    {
                        tempValSid = "top";
                    }
                }
                else if (tGeomPntY1 > boxBRY) 
                {
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,parseFloat(10000000000000),boxBRY,parseFloat(-10000000000000),boxBRY);
                    
                    tempYValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempYValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntY1 < tempYValY)
                    {
                        tempYValX = "NA";
                        tempYValY = "NA";
                    }
                    else
                    {
                        tempValSid = "bot";
                    }
                }
                
                if ((tempXValX == "NA") && (tempXValY == "NA") && (tempYValX == "NA") && (tempYValY == "NA"))
                {
                    window.alert("NA1");
                }
                else if ((tempXValX == "NA") && (tempXValY == "NA"))
                {
                    tempXArr[tempXArr.length] = tempYValX;
                    tempYArr[tempYArr.length] = tempYValY;
                }
                else if ((tempYValX == "NA") && (tempYValY == "NA"))
                {
                    tempXArr[tempXArr.length] = tempXValX;
                    tempYArr[tempYArr.length] = tempXValY;
                }
                else                
                {
                    if (((tempXValX == boxTLX) && (tempXValY == boxTLY)) || ((tempXValX == boxTLX) && (tempXValY == boxBRY)) || ((tempXValX == boxBRX) && (tempXValY == boxTLY)) || ((tempXValX == boxBRX) && (tempXValY == boxBRY)))
                    {                        
                        tempXArr[tempXArr.length] = tempXValX;
                        tempYArr[tempYArr.length] = tempXValY;
                        
                        tempXArr[tempXArr.length] = tempYValX;
                        tempYArr[tempYArr.length] = tempYValY;
                    }
                    else if (((tempYValX == boxTLX) && (tempYValY == boxTLY)) || ((tempYValX == boxTLX) && (tempYValY == boxBRY)) || ((tempYValX == boxBRX) && (tempYValY == boxTLY)) || ((tempYValX == boxBRX) && (tempYValY == boxBRY)))
                    {
                        tempXArr[tempXArr.length] = tempYValX;
                        tempYArr[tempYArr.length] = tempYValY;
                        
                        tempXArr[tempXArr.length] = tempXValX;
                        tempYArr[tempYArr.length] = tempXValY;
                    }
                }
            }
            
            if (tGeomStatP2 == "out")
            {    
                var tempXValX = "NA";
                var tempXValY = "NA";
                var tempYValX = "NA";
                var tempYValY = "NA";
                
                if (tGeomPntX2 < boxTLX) 
                {                    
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxTLX,parseFloat(10000000000000),boxTLX,parseFloat(-10000000000000));
                    
                    tempXValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempXValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntX2 > tempXValX)
                    {
                        tempXValX = "NA";
                        tempXValY = "NA";
                    }
                    else
                    {
                        tGeomLastSid = "left";
                    }
                }
                else if (tGeomPntX2 > boxBRX) 
                {
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,boxBRX,parseFloat(10000000000000),boxBRX,parseFloat(-10000000000000));
                    
                    tempXValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempXValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntX2 < tempXValX)
                    {
                        tempXValX = "NA";
                        tempXValY = "NA";
                    }
                    else
                    {
                        tGeomLastSid = "right";
                    }
                }  
                if (tGeomPntY2 < boxTLY) 
                {
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,parseFloat(10000000000000),boxTLY,parseFloat(-10000000000000),boxTLY);
                    
                    tempYValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempYValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntY2 > tempYValY)
                    {
                        tempYValX = "NA";
                        tempYValY = "NA";
                    }
                    else
                    {
                        tGeomLastSid = "top";
                    }
                }
                else if (tGeomPntY2 > boxBRY) 
                {
                    var tLineInterArray = lineIntersect(tGeomPntX1,tGeomPntY1,tGeomPntX2,tGeomPntY2,parseFloat(10000000000000),boxBRY,parseFloat(-10000000000000),boxBRY);
                    
                    tempYValX = convert2MinMax(Math.round(tLineInterArray[0]),boxTLX,boxBRX);
                    tempYValY = convert2MinMax(Math.round(tLineInterArray[1]),boxTLY,boxBRY);
                    
                    if (tGeomPntY2 < tempYValY)
                    {
                        tempYValX = "NA";
                        tempYValY = "NA";
                    }
                    else
                    {
                        tGeomLastSid = "bot";
                    }
                }
                
                if ((tempXValX == "NA") && (tempXValY == "NA") && (tempYValX == "NA") && (tempYValY == "NA"))
                {
                   window.alert("NA2");
                }
                else if ((tempXValX == "NA") && (tempXValY == "NA"))
                {
                    tempXArr[tempXArr.length] = tempYValX;
                    tempYArr[tempYArr.length] = tempYValY;
                }
                else if ((tempYValX == "NA") && (tempYValY == "NA"))
                {
                    tempXArr[tempXArr.length] = tempXValX;
                    tempYArr[tempYArr.length] = tempXValY;
                }
                else                
                {
                    if (((tempXValX == boxTLX) && (tempXValY == boxTLY)) || ((tempXValX == boxTLX) && (tempXValY == boxBRY)) || ((tempXValX == boxBRX) && (tempXValY == boxTLY)) || ((tempXValX == boxBRX) && (tempXValY == boxBRY)))
                    {
                        tempXArr[tempXArr.length] = tempYValX;
                        tempYArr[tempYArr.length] = tempYValY; 
                        
                        tempXArr[tempXArr.length] = tempXValX;
                        tempYArr[tempYArr.length] = tempXValY;
                    }
                    else if (((tempYValX == boxTLX) && (tempYValY == boxTLY)) || ((tempYValX == boxTLX) && (tempYValY == boxBRY)) || ((tempYValX == boxBRX) && (tempYValY == boxTLY)) || ((tempYValX == boxBRX) && (tempYValY == boxBRY)))
                    {
                        tempXArr[tempXArr.length] = tempXValX;
                        tempYArr[tempYArr.length] = tempXValY; 

                        tempXArr[tempXArr.length] = tempYValX;
                        tempYArr[tempYArr.length] = tempYValY;
                    }
                }
            }                  
            else if (tGeomStatP2 == "in")
            {
                //tempXArr[tempXArr.length] = parseInt(tGeomPntX2);
                //tempYArr[tempYArr.length] = parseInt(tGeomPntY2);
            }
        }
        else
        {
            tempXArr[tempXArr.length] = parseInt(tGeomPntX1);
            tempYArr[tempYArr.length] = parseInt(tGeomPntY1);            
            
            if (k == lineArrX.length-2)
            {                    
                tempXArr[tempXArr.length] = convert2MinMax(parseInt(tGeomPntX2),boxTLX,boxBRX);
                tempYArr[tempYArr.length] = convert2MinMax(parseInt(tGeomPntY2),boxTLY,boxBRY);
            }
        }        
    }
   
    if ((lineShape == "poly") && ((tempXArr[0] != tempXArr[tempXArr.length-1]) || (tempYArr[0] != tempYArr[tempYArr.length-1])))
    {
        tempXArr[tempXArr.length] = tempXArr[0];
        tempYArr[tempYArr.length] = tempYArr[0];
    }
   
    var cutLine2BoxArr=new Array();
		cutLine2BoxArr[0]=tempXArr;
		cutLine2BoxArr[1]=tempYArr;	
	return cutLine2BoxArr;
}
function utm2utm(wx, wy, zn)
{
	var LatLonArr=utm2latlon(wx, wy, zn);
		var nlat=LatLonArr[0];
		var nlon=LatLonArr[1];
	
	var UTMArr=latlon2utm(nlat, nlon);
		var nwx=UTMArr[0];
		var nwy=UTMArr[1];
		var nzn=UTMArr[2];
		
	var UTMUtilArr=new Array();
		UTMUtilArr[0]=nwx;
		UTMUtilArr[1]=nwy;
		UTMUtilArr[2]=nzn;	
	return UTMUtilArr;
}
function utm2latlon(wx, wy, zn)
{	
	utmNorthing=wy;
	utmEasting=wx;
	utmZone=zn;
	                
	var   k0 = 0.9996;
	var   a = 6378137;
	var   eccSquared = 0.00669438;
	var   eccPrimeSquared;
	var   e1 = parseFloat((1.0-Math.sqrt(1.0-eccSquared))/(1.0+Math.sqrt(1.0-eccSquared)));
	var   N1, T1, C1, R1, D, M;
	var   lngOrigin;
	var   mu, phi1, phi1Rad;
	var   x, y;
	var   zoneNumber;
	var rad2deg = parseFloat(180.0 / Math.PI);

	x = utmEasting - 500000.0;
	y = utmNorthing;

	zoneNumber = utmZone;
	             
	lngOrigin = (zoneNumber - 1)*6 - 180 + 3;

	eccPrimeSquared = (eccSquared)/(1-eccSquared);

	M = y / k0;
	mu = M/(a*(1-eccSquared/4-3*eccSquared*eccSquared/64-5*eccSquared*eccSquared*eccSquared/256));

	phi1Rad = parseFloat(mu    + (3.0*e1/2.0-27.0*e1*e1*e1/32.0)*Math.sin(2.0*mu) 
	                  + (21.0*e1*e1/16.0-55.0*e1*e1*e1*e1/32.0)*Math.sin(4.0*mu)
	                  +(151.0*e1*e1*e1/96.0)*Math.sin(6.0*mu));
	phi1 = phi1Rad*rad2deg;

	N1 = parseFloat(a/Math.sqrt(1-eccSquared*Math.sin(phi1Rad)*Math.sin(phi1Rad)));
	T1 = parseFloat(Math.tan(phi1Rad)*Math.tan(phi1Rad));
	C1 = parseFloat(eccPrimeSquared*Math.cos(phi1Rad)*Math.cos(phi1Rad));
	R1 = parseFloat(a*(1.0-eccSquared)/Math.pow(1.0-eccSquared*Math.sin(phi1Rad)*Math.sin(phi1Rad), 1.5));
	D = x/(N1*k0);
	                
	lat = parseFloat(phi1Rad - (N1*Math.tan(phi1Rad)/R1)*
	              (D*D/2.0-
	               (5.0+3.0*T1+10.0*C1-4.0*C1*C1-
	                9.0*eccPrimeSquared)*D*D*D*D/24.0
	               +(61.0+90.0*T1+298.0*C1+45.0*T1*T1-
	                 252.0*eccPrimeSquared-3.0*C1*C1)*D*D*D*D*D*D/720.0));                

	lat = lat * rad2deg;

	lng = parseFloat((D-(1.0+2.0*T1+C1)*D*D*D/6.0+
	                (5.0-2.0*C1+28.0*T1-3.0*C1*C1+
	                 8.0*eccPrimeSquared+24.0*T1*T1)
	                *D*D*D*D*D/120.0)/Math.cos(phi1Rad));                                                
	            
	lng = lngOrigin + lng * rad2deg;
	
	var LatLonArr=new Array();
		LatLonArr[0]=lat;
		LatLonArr[1]=lng;	
	return LatLonArr;
}
function latlon2utm(lat, lng)
{
	Lat=lat;
	Long=lng;
		
	with (Math)
	{
		Deg2Rad=PI/180.0;

		F0=0.9996;
		A1=6378137.0*F0;
		B1=6356752.3142*F0;
		K0=0;
		N0=0;
		E0=500000;

		N1=(A1-B1)/(A1+B1);
		N2=N1*N1; N3=N2*N1;
		E2=((A1*A1)-(B1*B1))/(A1*A1);

		K=Lat*Deg2Rad; L=Long*Deg2Rad;
		SINK=sin(K); COSK=cos(K); TANK=SINK/COSK; TANK2=TANK*TANK;
		COSK2=COSK*COSK; COSK3=COSK2*COSK;
		K3=K-K0; K4=K+K0;

		Merid=floor((Long)/6)*6+3;
		if ((Lat>=72) && (Long>=0))
		{
			if (Long<9) Merid=3; else if (Long<21) Merid=15; else if (Long<33) Merid=27; else if (Long<42) Merid=39;
		}
		if ((Lat>=56) && (Lat<64))
		{
			if ((Long>=3) && (Long<12)) Merid=9;
		}
		MeridWest=Merid<0;
		if (MeridWest) {MeridValue="W";} else {MeridValue="E";}
		MeridianForm=abs(Merid);
		L0=Merid*Deg2Rad;
			
		J3=K3*(1+N1+1.25*(N2+N3));
		J4=sin(K3)*cos(K4)*(3*(N1+N2+0.875*N3));
		J5=sin(2*K3)*cos(2*K4)*(1.875*(N2+N3));
		J6=sin(3*K3)*cos(3*K4)*35/24*N3;
		M=(J3-J4+J5-J6)*B1;

		Temp=1-E2*SINK*SINK;
		V=A1/sqrt(Temp);
		R=V*(1-E2)/Temp;
		H2=V/R-1.0;

		P=L-L0; P2=P*P; P4=P2*P2;
		J3=M+N0;
		J4=V/2*SINK*COSK;
		J5=V/24*SINK*(COSK3)*(5-(TANK2)+9*H2);
		J6=V/720*SINK*COSK3*COSK2*(61-58*(TANK2)+TANK2*TANK2);
		North=J3+P2*J4+P4*J5+P4*P2*J6;
			   
		J7=V*COSK;
		J8=V/6*COSK3*(V/R-TANK2);
		J9=V/120*COSK3*COSK2;
		J9=J9*(5-18*TANK2+TANK2*TANK2+14*H2-58*TANK2*H2);
		East=E0+P*J7+P2*P*J8+P4*P*J9;
		IEast=round(East); INorth=round(North);
			
		Easting=IEast;
		Northing=INorth;

		EastStr=""+abs(IEast); NorthStr=""+abs(INorth);
		while (EastStr.length<7) EastStr="0"+EastStr;
		while (NorthStr.length<7) NorthStr="0"+NorthStr;
		GR100km=eval(EastStr.substring(1,2)+NorthStr.substring(1,2));

		LongZone=(Merid-3)/6+31;
		if (LongZone % 1 != 0)
		{
			GR="false";
		}	
		else
		{
			if (IEast<100000 || Lat<-80 || IEast>899999 || Lat>=84)
			{
				GR="false";
				Zone=1;
			}	
			else
		    {
				Pos=round(Lat/8-0.5)+10+2;

				Pos=round(abs(INorth)/100000-0.5);
				while (Pos>19) Pos=Pos-20;
							
				if (LongZone % 2 == 0)
				{ 
					Pos=Pos+5; if (Pos>19) Pos=Pos-20; 
				}
				Pos=GR100km/10-1;
				P=LongZone; 
				while (P>3) P=P-3;
				Pos=Pos+((P-1)*8);

				Zone=LongZone;
		    }
		}
	}
        
	wx = Easting;
	wy = Northing;
	zn = Zone;
	
	var UTMArr=new Array();
		UTMArr[0]=wx;
		UTMArr[1]=wy;
		UTMArr[2]=zn;
	return UTMArr;
}
function feet2meters(ft)
{
	var m=ft*0.3048;
	return m;
}
function miles2meters(ml)
{
	var m=ml*1609.344;
	return m;
}
function meters2feet(m)
{
	var ft=m*3.2808399;
	return ft;
}
function meters2miles(m)
{
	var ml=m*0.000621371192;
	return ml;
}
function feet2miles(ft)
{
	var ml=ft/5280;
	return ml;
}
function miles2feet(ml)
{
	var ft=ml*5280;
	return ft;
}
function acres2sqmeters(ac)
{
	var sqm=ac*4046.85642;
	return sqm;
}
function sqmeters2hectares(sqm)
{
	var hc=sqm/10000;
	return hc;
}