#!/usr/bin/env python
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology! 
Clean code is much better than Cleaner comment
'''

import glob
import sys
import os

from functools import partial


cpath = os.path.dirname(os.path.abspath(__file__))
ml = partial(os.path.relpath, start=cpath)


def main():
    '''Simple and funny'''
    l1 = glob.glob('%s/*.*' % cpath)
    l2 = glob.glob('%s/*/*.*' % cpath)
    l3 = glob.glob('%s/*/*/*.*' % cpath)
    files = map(ml, l1) + map(ml, l2) + map(ml, l3)
    if 'cache.cfm' in files:
        files.remove('cache.cfm')
    cache_manifest = 'CACHE MANIFEST\n{file_list}\nNETWORK:\n*\nFALLBACK:\n/ img/offline.png'
    file_string_list = '\n'.join(files)
    data = cache_manifest.format(file_list=file_string_list)
    with open('%s/cache.cfm' % cpath, 'w') as cfm:
        cfm.write(data)
    print '\tCache manifest generated.'

if __name__ == '__main__':
    main()



