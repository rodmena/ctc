/*
 * Main.c
 *
 *  Created on: 23.02.2014
 *      @Author: Farsheed Ashouri
 *      @copyright: Chista Co.
 *    ___              _                   _ 
 *   / __\_ _ _ __ ___| |__   ___  ___  __| |
 *  / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
 * / / | (_| | |  \__ \ | | |  __/  __/ (_| |
 * \/   \__,_|_|  |___/_| |_|\___|\___|\__,_|
 *                                         
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "Common.h"  /*prototypes here*/
#include "PointInPolygon.h"
//#include <pthread.h>
//#include <omp.h>

//#define NTHREADS 4

unsigned long count_lines_of_file(char *file_patch) {


    FILE* myfile = fopen(file_patch, "r");
    int ch, number_of_lines = 0;
    do 
    {
        ch = fgetc(myfile);
        if(ch == '\n')
            number_of_lines++;
    } while (ch != EOF);

    /*
    * last line doesn't end with a new line!
    * but there has to be a line at least before the last line
    */

    // if(ch != '\n' && number_of_lines != 0) 
    // number_of_lines++;
    

    fclose(myfile);
    /*printf("number of lines in test.txt = %d\n", number_of_lines);*/
    return number_of_lines;
}

static int get_file_size(FILE *fp){

    fseek(fp, 0, SEEK_END);
    int size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return size;

}

static Point *Get_Points(FILE *fp)
{
    /**************************************/
    /*      Generating the polygon        */
    /**************************************/

	Point* points;
    int size = get_file_size(fp);
    int fplen = size/sizeof(Point);
	points = myMalloc(size); /*Allocate memory for main polygon*/
    //Point ITA[size];
    int i;
    for (i = 0; i < fplen; i++) {
        Point tmp;
        if( fread(&tmp, sizeof(Point), 1, fp) )
            points[i] = tmp;
        //printf("%d, %d,%d\n",counter ,tmp.x, tmp.y);
    }
    return points;
}


int check_point(int x, int y, char* polygonfile){

    FILE *fp;  /*border*/
    fp = fopen(polygonfile, "rb");  /* Simply read the file */
	if (!fp)
	    {
		fprintf(stderr, "Unable to open file %s\n", polygonfile);
        exit(1);
        }

	Point* polygon;
    Point testPoint;
    testPoint.x = x;
    testPoint.y = y;
    polygon = Get_Points(fp);
    int size = get_file_size(fp);
    fclose(fp);
    int fplen = size/sizeof(Point);

    /******************
     * 1: Inside
     * 2: on edge
     * 3: on tile
     *****************/
    int result;
    result = isInPolygon(testPoint, polygon, fplen);

	if (polygon != 0) {
		free(polygon);
	}

    return result;
}

void check(int ftlen, int fplen, Point *polygon, Point *tiles, FILE *fo){
	Point* poly;
    int counter;
    int i, j;
    for (i=0; i<ftlen; i++){
        Point testPoint;
        testPoint = tiles[i];
    /*************************************/
        poly = myMalloc(fplen*sizeof(Point));
        for (j=0;j<fplen;j++)
        {
            /* recreate a temp poly */
            Point tp;
            int x = polygon[j].x;
            int y = polygon[j].y;
            tp.x = x;
            tp.y = y;
            poly[j]=tp;  /*Ok now we have a copy */
        }
    /************************************/
        if (isInPolygon(testPoint, poly, fplen) > 0) {
            //printf(".");
            //fprintf (fo, "%d,%d\n", testPoint.x, testPoint.y);  /****4****/
            counter +=1 ;
            if( fwrite (&testPoint, sizeof(Point), 1, fo) ){

                //write something if you want
                    
                }/* Write BINARY out this point */
        } else {

            //printf ("%d,%d\n", testPoint.x, testPoint.y);  /****4****/
            //printf("OUTSIDE\n");
        }

	if (poly != 0) {
		free(poly); // remove from heap
	}
        }
}

int bulk_check(char* polygonfile, char* tileslist, char* outputfile) {
    /*Some variables*/
    FILE *fp;  /*border*/
    FILE *ft;  /*tiles*/
    FILE *fo;  /*output*/
	Point* polygon;
    Point* tiles;
	//Point* outpoly;
    fp = fopen(polygonfile, "rb");  /* Simply read file in ascii mode */
	if (!fp)
	    {
		fprintf(stderr, "ctcpip error: Unable to open file %s\n", polygonfile);
        exit(1);
        }

    ft = fopen(tileslist, "rb");  /* Simply read file in ascii mode */
	if (!ft)
	{
		fprintf(stderr, "ctcpip error: Unable to open file %s\n", tileslist);
        return 0;
    }
    fo = fopen(outputfile, "wb");  /* open a file for output*/

    if (fo == NULL)
    {
        printf("ctcpip error: Unable to open file!\n");
        exit(1);
    }

    //clfp = count_lines_of_file(polygonfile);
	//outpoly = myMalloc(clft * sizeof(Point)); /*Allocate memory for main polygon*/



    /*********** get polygon  ************/
    int size = get_file_size(fp);
    int fplen = size/sizeof(Point);
    polygon = Get_Points(fp);


    /*************************************/

        size = get_file_size(ft);
        int ftlen = size/sizeof(Point);
	    tiles = Get_Points(ft);

        //printf("\n\tCTC PiP going to verify \033[93m%d\033[0m tile(s)", ftlen);
        {
            check(ftlen, fplen, polygon, tiles, fo);
        } // end openmp parallel processing part
    /*Clean Up*/
    fclose(fo);
    fclose(ft);
    fclose(fp);
	if (polygon != 0) {
		free(polygon);
	}
	if (tiles != 0) {
		free(tiles);
	}
    //printf("\t Approved \033[92m%d\033[0m", counter);
    //printf("\n");
    return 1;
}



int main(int argc, char* argv[]){

	char* correctUsage = "PointInPolygon <polygon file> <input tiles> <output file>\n";
    char* polygonfile;
    char* tileslist;
    char* outputfile;
    int numofargs = argc-1 ;
    if (numofargs<3) printf("\tusage: %s", correctUsage);

    polygonfile = argv[1];  /*Get polygon file path from arguments;*/
    tileslist = argv[2];  /*Get file path for tiles */
    outputfile = argv[3];  /*Get output file path*/

     bulk_check(polygonfile, tileslist, outputfile);  /*Simply run the go function*/
	 exit(EXIT_SUCCESS);
}
