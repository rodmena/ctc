#!/usr/bin/env python

"""
setup.py file for SWIG example


python setup.py build_ext --inplace


"""

from distutils.core import setup, Extension


PointInPolygon_module = Extension('_PointInPolygon',
                           sources=['Common.c', 'PointInPolygon.c', 'PointInPolygon_wrap.c', 'Main.c'],
                           extra_compile_args=["-Wno-deprecated","-O3"],
                           )

setup (name = 'PointInPolygon',
       version = '0.1',
       author      = "SWIG Docs",
       description = """Simple swig example from docs""",
       ext_modules = [PointInPolygon_module],
       py_modules = ["PointInPolygon"],
       )
