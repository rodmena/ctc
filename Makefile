#/**************************************************************/
# -- CTC python plugins Makefile --
# @copyright: Chista Co.
# @author: Farsheed Ashouri
# #/**************************************************************/

PYV=$(shell ../ctcdev/bin/python -c "import sys;t='{v[0]}.{v[1]}'.format(v=list(sys.version_info[:2]));sys.stdout.write(t)");

test:

	make build;
	make clean;

build:
	
	swig -Wall -O -builtin -python src/point_in_polygon/PointInPolygon.i
	c99 -O3 -fPIC -ffast-math -c src/point_in_polygon/Common.c src/point_in_polygon/PointInPolygon.c src/point_in_polygon/Main.c src/point_in_polygon/PointInPolygon_wrap.c -I../ctcdev/include/python$(PYV)
	c99 -O3 -ffast-math -shared Common.o PointInPolygon.o Main.o PointInPolygon_wrap.o -o ctc/_PointInPolygon.so

clean:
	rm -f *.o
	rm -f PointInPolygon.py
	rm -f PointInPolygon_wrap.c
