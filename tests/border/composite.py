#!/usr/bin/env python

import Image
import ImageChops
import hashlib
def composite(bg, fr, mask, output='over.jpg'):
    '''
        composite two images based on a black&white mask.
    '''
    im1 = Image.open(fr)
    im2 = Image.open(bg)
    ### Process
    border = Image.open(mask)
    mask = border.split()[-1]
    #im2.putalpha(mask)
    #im1.paste(im2, (0, 0), mask)
    ### End of process
    x = ImageChops.composite(im1, im2, mask)
    #print hashlib.md5(x).hexdigest()
    x.save(output)


if __name__ == '__main__':

    composite('image1.jpg', 'image2.jpg', 'border.jpg')

