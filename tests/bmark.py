


'''
Install:
    sudo apt-get install -y build-essential python-dev
    sudo pip install gevent grequests 

'''

import gevent.monkey
gevent.monkey.patch_all()
import grequests
import sys
import time

def x(*args, **kw):
    print >> sys.stderr, '.',

def test_grequests(mode, ccn, path):
    print 'testing with Grequests'
    data = open(path, 'r').readlines()
    s = time.time()
    urls = list()
    for url in data:
        #print >> sys.stderr, '.',
        r = grequests.AsyncRequest(
            mode, url, callback=x)
        urls.append(r)
    grequests.map(urls, size=ccn)
  
    print '\n'
    print time.time() - s


if __name__ == '__main__':
    if len(sys.argv)<4:
        print '\tpython -OO bmark.py <mode> <ccn> <urls_list>'
        sys.exit()
    num = int(sys.argv[2])
    path = str(sys.argv[3])
    mode = str(sys.argv[1])
    test_grequests(mode, num, path)

