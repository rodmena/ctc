

## Just some tests

from PIL import Image
import math






def level_dic():
    '''
    http://wiki.openstreetmap.org/wiki/Zoom_levels
    '''
    #data = dict()
    # for i in range(20):
    #data[i] = 360.0 / (2 ** i)
    # print data
    # return data
    data = {0: 360.0,
            1: 180.0,
            2: 90.0,
            3: 45.0,
            4: 22.5,
            5: 11.25,
            6: 5.625,
            7: 2.813,
            8: 1.406,
            9: 0.703,
            10: 0.352,
            11: 0.176,
            12: 0.088,
            13: 0.044,
            14: 0.022,
            15: 0.011,
            16: 0.005,
            17: 0.003,
            18: 0.001,
            19: 0.0005}

    return data


def _c3857t4326(lon, lat):
    """
    My Pure python 3857 -> 4326 transform. About 12x faster than pyproj. :)
    """
    xtile = lon / 111319.49079327358
    ytile = math.degrees(
        math.asin(math.tanh(lat / 20037508.342789244 * math.pi)))
    return(xtile, ytile)

def getzoom(minx, miny, maxx, maxy):
    '''Calculate zoom for a bbox. Assumes tlesize is 256x256'''
    data = level_dic()  # our presets
    a, b = _c3857t4326(minx, miny)
    c, d = _c3857t4326(maxx, maxy)
    r = 3
    dne = abs(round(float(c) - float(a), r))  # ne: North East point
    mylist = [round(i, r) for i in data.values()] + [dne]
    new = sorted(mylist, reverse=True)
    # print new, dne
    return new.index(dne)

def deg2num(lat_deg, lon_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
  return (xtile, ytile)


def get_tiles_inside(minx, miny, maxx, maxy):
    z = getzoom(minx, miny, maxx, maxy)
    minx, miny = _c3857t4326(minx, miny)
    maxx, maxy = _c3857t4326(maxx, maxy)
    ax, ay = deg2num(miny, minx, z)
    bx, by = deg2num(maxy, maxx, z)
    tiles = set()
    for i in xrange(int(min(ax, bx)),int(max(ax, bx)+1)):
        for j in xrange(int(min(ay, by)),int(max(ay, by)+1)):
            point = (i, j, z)
            tiles.add(point)
    return list(tiles)

def get_area(tiles):
    xlist, ylist, _ = zip(*tiles)
    x = (max(xlist)-min(xlist)+1) * 256.0
    y = (max(ylist)-min(ylist)+1) * 256.0
    return x, y

def get_meter_per_pixel(minx, miny, maxx, maxy):
    '''http://wiki.openstreetmap.org/wiki/Zoom_levels'''
    z = getzoom(minx, miny, maxx, maxy)
    minx, miny = _c3857t4326(minx, miny)
    maxx, maxy = _c3857t4326(maxx, maxy)
    #ax, ay = deg2num(miny, minx, z)
    #bx, by = deg2num(maxy, maxx, z)
    C = 40075000
    S = C*math.cos(miny)/2**(z+7)
    return abs(S)
    

if __name__ == '__main__':
    
    #minx, miny, maxx, maxy = 5713317.7760771,4250645.638733,5738107.7846221,4260189.792022
    minx, miny, maxx, maxy = 5724822.2660022,4255353.7922687,5727466.5335804,4257734.6660061
    tiles = get_tiles_inside(minx, miny, maxx, maxy)
    print tiles
    x, y = get_area(tiles)
    center_pixel = x/2, y/2
    print get_meter_per_pixel(minx, miny, maxx, maxy)
    


