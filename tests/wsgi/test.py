from cgi import parse_qs, escape
import sys
sys.path.append('../../site-packages')
import bjoern

def hello_world(environ, start_response):
    parameters = parse_qs(environ.get('QUERY_STRING', ''))
    print parameters
    if 'subject' in parameters:
        subject = escape(parameters['subject'][0])
    else:
        subject = 'World'
    start_response('200 OK', [('Content-Type', 'text/html')])
    return ['''Hello %(subject)s
    Hello %(subject)s!

''' % {'subject': subject}]

if __name__ == '__main__':
    bjoern.run(hello_world, '127.0.0.1', 5090)
