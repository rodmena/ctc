import multiprocessing
NUMBER_OF_PROCESSES = multiprocessing.cpu_count()
def f(host,i):
    #l.acquire()
    print 'hello world', i
    #l.release()
    return 'STOP'

def worker(input, output):
    for host,text in iter(input.get, 'STOP'):
        output.put(f( host,text ))

if __name__ == '__main__':
    lock = multiprocessing.Lock()
    task_queue = multiprocessing.Queue()
    done_queue = multiprocessing.Queue()
    for h in range( NUMBER_OF_PROCESSES ):
        t = (h,h)
        task_queue.put(t)
    for host in range(NUMBER_OF_PROCESSES):
        multiprocessing.Process(target=worker,  args=(task_queue, done_queue)).start()

    print 'done.'
