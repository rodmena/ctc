

import requests
import simplejson as sj
import sys

baseurl = 'http://map.tehran.ir/searchapi/search.php?action=se&key=5e53751837bf2b23af13bcc288006bc0&query={query}&bbox={bbox}&co={co}'

bbox = '505737 3924261 ,563901 3924261,563901 3965398,505737 3965398,505737 3924261'
query = sys.argv[1]
co = sys.argv[2]
url = baseurl.format(query=query, bbox=bbox, co=co)
r = requests.get(url)
if r.status_code == 200:
    content = r.content
    data = sj.loads(content)
    print 'real results: ', data.get('results')
    locations = data.get('locations')
    if locations:
        for i in locations:
            print i.get('label')
    sponsors = data.get('sponsors')
    print 'Sponsers: ', len(sponsors)
    if sponsors:
        for i in sponsors:
            print i.get('label')


