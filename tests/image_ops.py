from PIL import Image
import cProfile as profile

def a():
    img = Image.open('Mask2.png')
    datas = img.getdata()

    newData = []
    for item in datas:
        threshold = 20
        #remove white
        if item[0] > threshold and item[1] > threshold and item[2] > threshold:
            newData.append((0, 0, 0))
        else:
            newData.append(item)

    img.putdata(newData)
    img.show()

a()