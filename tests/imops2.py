from PIL import Image, ImageMath, ImageFilter 
import time

image = Image.open('Mask2.png')


def difference1(source, color):
    """When source is bigger than color"""
    return ((255 - color) / source - color)

def difference2(source, color):
    """When color is bigger than source"""
    return color / (color - source)


def color_to_L(image, color=None):
    width, height = image.size
    color = map(float, color)
    img_bands = [band.convert("F") for band in image.split()]
    alpha = ImageMath.eval(
        """float(
            max(
                max(
                    max(
                        difference1(red_band, cred_band),
                        difference1(green_band, cgreen_band)
                    ),
                    difference1(blue_band, cblue_band)
                ),
                max(
                    max(
                        difference2(red_band, cred_band),
                        difference2(green_band, cgreen_band)
                    ),
                    difference2(blue_band, cblue_band)
                )
            )
        )""",
        difference1=difference1,
        difference2=difference2,
        red_band = img_bands[0],
        green_band = img_bands[1],
        blue_band = img_bands[2],
        cred_band = color[0],
        cgreen_band = color[1],
        cblue_band = color[2]
    )

    a = ImageMath.eval("convert(255 * alpha, 'L')", alpha = alpha)
    new_bands = [a]

    return Image.merge('L', new_bands)

def get_color(color):
    if color == 'white':
        return (255,255,255)
    if color == 'red':
        return (255,0,0)
    if color == 'green':
        return (0,255,0)
    if color == 'blue':
        return (0,0,255)
    if color == 'magenta':
        return (255,0,255)
    if color == 'yellow':
        return (255,255,0)
    if color == 'cyan':
        return (0,255,255)

def mask(im, color='red'):
    w,h=im.size
    s = time.time()
    color = get_color(color)
    image = color_to_L(im, color)
    image.filter(ImageFilter.MedianFilter(size=9)).show()
    print time.time()-s


import cProfile
cProfile.run("mask(image, 'white')")


