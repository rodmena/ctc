#!/bin/bash

copyright="Chista Co."
#
#    ___              _                   _ 
#   / __\_ _ _ __ ___| |__   ___  ___  __| |
#  / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
# / / | (_| | |  \__ \ | | |  __/  __/ (_| |
# \/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

#Just remember: Each comment is like an apology! 
#Clean code is much better than Cleaner comments!



VERSION=`cat VERSION`
echo "*** CTC $VERSION Installation ***"
echo "** Copyright of Chista Co. 2013-14 | All rights reserved."
echo "** Welcome to CTC Installation. Please wait to update your repositories first:"
sudo apt-get update
echo "** CTC needs some basic ubuntu packages. Like python-dev and compile tools."
echo "** Also we need to install NGINX, virtualenv and some libraries for python. (~52 Mb download)"
echo "** Please wait to install those:"
sudo apt-get install -y build-essential python python-dev swig nginx \
        python-virtualenv libjpeg-dev libpng-dev libtiff-dev libfreetype6-dev \
        python-setuptools imagemagick redis-server rabbitmq-server
echo "** Done. Now let's setup a virtual environment for ctc:"
CTCPATH=$PWD
cd $CTCPATH/..
if [ ! -d ctcdev ]
    then
        virtualenv ctcdev
fi
cd ctcdev
echo "** Activating virtualenv ..."
source bin/activate
echo "** Installing python packages ..."
cd $CTCPATH
tar xf packages.tar.gz 
echo "** Installing main python modules ..."
pip install --index-url=file://$CTCPATH/packages/simple -r $CTCPATH/requirements.txt
pip install utm ##Not ok. hard coded. must remove later
rm -rf packages/
#pip install -U -r $CTCPATH/requirements.txt
echo "** Making default cache directory"
sudo mkdir -p /usr/local/ctc/cache
sudo chmod 777 -R /usr/local/ctc
echo "** Create a shortcut of cache directory on your desktop"
if [ ! -d $HOME/Desktop/CTC-Caches ]
	then 
		ln -s /usr/local/ctc/cache $HOME/Desktop/CTC-Caches
fi
cd $CTCPATH
make
cd $CTCPATH/bin
echo "** generating NGINX/uWsgi config files ..."
python generate_serving_configs.py
python ../app/static/cache.py
chmod +x ctcseed
chmod +x ctcshow
chmod +x ctcclean
chmod +x ctclog
chmod +x ctcupdate
chmod +x ctctop

if [ -f /etc/nginx/sites-enabled/default ]
    then sudo rm -f /etc/nginx/sites-enabled/default
fi

echo "** Creating shortcuts ..."
sudo ln -s -f $CTCPATH/bin/ctc* /usr/local/bin/
echo "** Copying config files ..."
sudo ln -s -f $CTCPATH/bin/nginx/ctc.conf /etc/nginx/sites-enabled/ctc.conf
sudo ln -s -f $CTCPATH/bin/nginx/gzip*.conf /etc/nginx/conf.d/

if [ -f /etc/init.d/uwsgi ]
	then
	sudo rm -f /etc/init.d/uwsgi
fi
sudo cp uwsgi.initd /etc/init.d/uwsgi
sudo chmod +x /etc/init.d/uwsgi
echo "Adding uwsgi to startup"
sudo update-rc.d uwsgi defaults
echo "** Done. Let's start the services ..."

sudo service uwsgi restart
sudo service nginx restart

echo "######################################################"
echo "####################  NOTICE #########################"
echo "## Please Check correct setting in config/ctc.ini ####"
echo "###### And double check your NGINX config file! ######"
echo "###################  THANK YOU! ######################"
echo "######################################################"
echo "** Done. Thats all!"
echo "** You can visit http://127.0.0.1:8000 to test CTC"
echo "** CTC $VERSION installed successfully."


