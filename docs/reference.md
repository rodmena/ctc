Chista Tile Cache (CTC) Reference
=================================
[Farsheed Ashouri](http://ourway.ir), [**Chista** Co](http://chista.ir/?p=about), March 2014 



The CTC project
---------------
**Chista Tile cache** provides a [Python](http://python.org)-based **WMS/TMS** server and developed by **Python** and [**C ANSI**](http://en.wikipedia.org/wiki/ANSI_C). In the simplest use case, **CTC** requires only write access to a disk and a valid wms/tms data source. Then it will act as our own map server. It included with various third party tools and works perfectly with [uWSGI](http://uwsgi-docs.readthedocs.org/en/latest/index.html) (The fastest app server) and [NGINX](http://nginx.com/) (The fastest static web server).

Preface
-------
This refrenced is written with respect to internal and technical aspects of CTC, so the programming/SaaS knowledge is required to follow the topics. 





