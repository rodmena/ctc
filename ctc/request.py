#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an appology!
Clean code is much better than Cleaner comments!
'''

import unittest
import os
import sys
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
import re
import time
import string
import math
import gevent.monkey
import gevent
gevent.monkey.patch_socket()
#gevent.monkey.patch_thread()
import urllib
import requests
from PIL import Image
from PIL import ImageChops  # for compositing borders.
from cStringIO import StringIO
import hashlib
from random import shuffle, choice
import ctc.utils as utils
import ctc.packages.tilenames as tilenames
from cgi import parse_qs
from blessings import Terminal
import threading
import io
import xml.etree.cElementTree as ET
no_uwsgi = False
try:
    import uwsgi
except ImportError:
    no_uwsgi = True
import ctc.projections as projections
import logging
logger = logging.getLogger('ctc')
apilog = logging.getLogger('api')
from greenlet import greenlet

term = Terminal()


class Request(object):

    ''' Main Request Class'''

    def __init__(self, config, session, rid, force=False):
        '''Get the parameters'''
        self.main_config = config
        self.force = force
        self.session = session
        self.rid = rid

    def parse(self, uri):
        '''Get input uri, parse it and prepare wms/tms parameters.'''
        self.uri = uri
        _, _, self.section, z, x, y = map(str.strip, uri)
        y, self.ext = map(str.strip, map(str.lower, y.split('.')))
        self.master_ext = self.ext
        try:
            self.z, self.x, self.y = int(z), int(x), int(y)
        except ValueError, e:
            return '400 Bad Request , %s' % e, 0
        self.tile = (self.x, self.y, self.z)
        x = 'service=WMS&version=2.1.1'
        x += '&request=GetMap&LAYERS={layer}&BBOX={bbox}&'
        x += 'WIDTH=256&HEIGHT=256&srs=EPSG:900913&FORMAT={format}'
        self.parameters = parse_qs(x)
        self.IO = False
        self.format = 'image/' + self.ext
        if self.ext == 'png':
            self.format = 'image/png8'
        if self.ext == 'jpg':
            self.format = 'image/jpeg'
        
        self.parameters['FORMAT'] = self.format

        self.timeout = 60  # default
        self.cache_header = 10
        self.config = dict()
        if self.section in self.main_config.keys():
            self.config = self.main_config[self.section]
            self.master = self.section
        else:
            return '400 Bad Request , Wrong Layer Name!', 0
        if self.config.get('client-max-age') and self.config.get('client-max-age')[0]:
            self.cache_header = int(self.config.get('client-max-age')[0])
        if self.config.get('timeout') and self.config.get('timeout')[0]:
            self.timeout = float(self.config.get('timeout')[0])
        force_format = self.config.get('force_format')
        if force_format and self.ext != force_format[0].strip():
            logger.error('400|{rid}|{section}|Wrong image format ({ext}). [FORCE MODE] Must be: {force}'
                         .format(
                             rid=self.rid, ext=self.ext, section=self.section,
                             force=force_format[0]))
            return '400 Bad Request , Wrong Image Format! Forced to be "%s"' % force_format[0].strip(), 0

        # convert tile to bbox
        try:
            self.bbox = projections.bbox(self.z, self.x, self.y)
        except Exception, e:
            logger.error(
                '500|{rid}|{e}||{z},{x},{y}|{uri}|'
                .format(rid=self.rid, x=self.x, y=self.y, z=self.z, e=e,
                        uri='/'.join(uri)))
            return '400 Bad Request , Not Correct Numbers for x,y,z', 0
        self.parameters['BBOX'][0] = self.bbox
        # print self.parameters
        self.gr = greenlet(self.go)
        return self.gr.switch()

    def __del__(self):
        '''Class bye-bye function'''
        #s = time.time()
        # print 'commit time: %s' % (time.time()-s)

    def error(self, text):
        '''custom error function'''
        error = '[{rid}] Error:{text}'.format(rid=self.rid, text=text)
        logger.error(error)
        return ('Error: %s' % text, 10)

    def doseq(self, parameters):
        '''Convert parameters as dict of lists and get a clean url param'''
        data = urllib.urlencode(parameters, doseq=True)
        return data

    def md5(self, data):
        '''Return md5 hash of the data'''
        return hashlib.md5(data).hexdigest()

    def fetch(self, url):
        '''fetch a url and return a response'''
        # print url
        logger.debug(
            '|{rid}|{section}|Fetcing Url||||{url}'
            .format(rid=self.rid, section=self.section, url=url))
        if not no_uwsgi:
            in_memory_proxies = uwsgi.cache_get('%s_proxies' % self.section)
            if in_memory_proxies:
                pr = utils.get_best_url('%s_proxies' % self.section)
                proxy = {'http': 'http://' + pr}
                debug = '{rid}|{section}|Proxy Used|{proxy}'\
                    .format(rid=self.rid, section=self.section, proxy=pr)
                logger.debug(debug)
                self.session.proxies = proxy
        # print utils.urls_details
        '''Get an image'''
        # print url
        headers = {

            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0',
        }
        if not 'maps.googleapis.com' in url:  # Not in google api mode
            headers['Referer'] = 'http://www.google.com'

        try:
            gf = greenlet(self.session.get)  # not a parent of self.gr
            r = gf.switch(url, timeout=self.timeout,
                          stream=False, headers=headers)
            reason = r.reason
            if r.status_code == 200:
                return r
            else:
                error = '{status}|{rid}|{section}|{reason}|{desc}|{x},{y},{z}|{url}'\
                    .format(rid=self.rid, status=r.status_code,
                            section=self.section, x=self.x, y=self.y,
                            z=self.z, url=url, reason=reason,
                            desc=utils.get_response(r.status_code)[0])
                logger.error(error)
                return '%s %s ,%s' % (r.status_code, reason, r.content)

        except requests.Timeout:
                error = '504|{rid}|{section}|Gateway Timeout|CTC can not wait more|{x},{y},{z}|{url}'\
                    .format(rid=self.rid, section=self.section,
                            x=self.x, y=self.y, z=self.z, url=url)
                return '504 Gateway Timeout ,CTC can not wait more'
        except requests.RequestException:
                error = '500|{rid}|{section}|Internal Server Error|CTC can not fetch|{x},{y},{z}|{url}'\
                    .format(rid=self.rid, section=self.section,
                            x=self.x, y=self.y, z=self.z, url=url)
                return '500 Internal Server Error ,CTC can not fetch!'
        except Exception, e:
            logger.error(
                '500|{rid}|{section}|{e}|{x},{y},{z}|{url}'
                .format(rid=self.rid, e=e, section=self.section, x=self.x,
                        y=self.y, z=self.z, url=url))
            return '500 Internal Server Error ,%s' % e

    def get_cache_folder(self):
        cache_folder = self.config.get('cache')
        if isinstance(cache_folder, list):
            cache_folder = cache_folder[0]
        return cache_folder

    def get_path(self, mode=1):
        '''
        return final resting place for tile cache files
        Mode 2 is for final comps.
        '''
        # postfix = '%s/%s/%s/%s'%('_resources', self.z, self.x, self.y)  ##
        # MISTAKE
        postfix = '_resources'
        prefix = '.'
        if mode == 2:
            postfix = ''
            prefix = ''
        cache_folder = self.get_cache_folder()
        if self.config.get('cache_exact_path') and self.config.get('cache_exact_path')[0] == 'true':
            cache_path=cache_folder
        else:
            cache_path = os.path.join(
                cache_folder, prefix + self.section + postfix)
        if not os.path.isdir(cache_path) and cache_folder.lower() != 'memory':
            try:
                os.makedirs(cache_path)
            except OSError:
                pass

        return cache_path

    def get_name(self, name):
        '''create a uniqe name based on tile and custom name'''
        base = '%s-%s%s%s-%s' % (utils.get_salt(),
                                 self.x, self.y, self.z, name)
        result = self.md5(base)
        return result

    def get_fpath(self, name, mode=1):
        '''Mode 1 is secion_resources'''
        tp = self.get_path(mode)
        if tp:
            fp = os.path.join(tp, name)
            fp = os.path.abspath(fp)
            if not os.path.isdir(os.path.dirname(fp)):
                try:
                    os.makedirs(os.path.dirname(fp))
                except OSError:
                    pass

            return fp

    def save(self, url, ext=None):
        '''Returns an Image buffer'''
        # print '\t{t.yellow}{t.bold}Rendering new tile{t.normal}: {t.blue}{bbox}{t.normal}'\
        #.format(t=term, bbox=self.bbox)
        if not ext:
            ext = self.ext
        #jobs = [gevent.spawn(self.fetch, url)]
        gf = greenlet(self.fetch)
        r = gf.switch(url)
        if not r:
            # return Image.open('data/images/500.jpg')
            return None
        if isinstance(r, requests.models.Response):
            data = r.content
            try:
                i = Image.open(StringIO(data))
                if i.size != (256, 256):
                    i = utils.crop_to_256(i)  # crop it centeraly
            except IOError, e:
                es = e
                try:  # try to find error code in wms data
                    error_data = ET.fromstring(r.content)
                    se = error_data.find("ServiceException")
                    error_code = se.get("code")
                    error_desc = se.text
                    es = "%s:%s" % (error_code, error_desc)
                except:
                    pass
                self.error(es)
                return '500 Internal Server Error ,CTC Cant recognize an image|%s' % es[:64]
        elif isinstance(r, Image.Image):
            return r  # Don't save, just return
        elif isinstance(r, str):
            return r
        #i = i.convert('RGB')
        #colors = list()
        # try:
        colors = utils.get_color_list(i, raw=True)
        # except Exception, e:
        #    pass

        if len(colors) == 1 and self.get_cache_folder().lower() != 'memory':  # ok, it's a solid color images, lets save it
            img = i
            save_format = self.ext.upper()
            if self.ext.lower() == 'jpg':
                img = img.convert('RGB')
                save_format = 'JPEG'
            if self.ext.lower() == 'png8':
                save_format = 'PNG'
            # if self.ext == 'png':
                #img = img.convert('P')
            #i = i.convert("P", palette=Image.ADAPTIVE, colors=1)
            color = colors[0]
            addr = "SOLID/%s.%s" % (color, ext.replace('png8', 'png'))
            fpath = self.get_fpath(addr, mode=2)  # No resources for now.
            if not os.path.isfile(fpath):
                img.save(fpath, save_format,
                         quality=self.get_quality(), optimize=True)
        return i

    def write(self, path, data):
        '''Saves a tile in disk'''
        s = time.time()
        with io.open(path, 'wb') as f:
            f.write(data)
            # uwsgi.wait_fd_write(f)
        # print '\t{t.cyan}{t.bold}Write Time: {s}{t.normal}'.format(t=term,
        # s=time.time() - s)
        return f.name

    def get_center_latlong(self):
        '''Calculate center of a tile in latlong format'''
        a, b, c, d = self.bbox.split(',')
        x = (float(a) + float(c)) / 2.0
        y = (float(b) + float(d)) / 2.0
        x, y = projections._c3857t4326(x, y)
        return (y, x)

    def get_tile(self):
        '''get tile number for a point in a zoom'''
        z = self.getzoom()
        a, b, c, d = self.bbox
        x = (float(a) + float(c)) / 2.0
        y = (float(b) + float(d)) / 2.0
        tile = tilenames.deg2num(y, x, z)
        return tile[0], tile[1], z

    def get_quality(self):
        '''Prepare quality setting for request'''
        if self.ext == 'png':
            # Dont touch it. Png is lossless. for best optmize, this MUST be 75
            # or even less
            return 85
        if self.config.get('quality'):
            return int(self.config.get('quality')[0])
        else:
            return 95

    def getzoom(self):
        '''Calculate zoom for a bbox. Assumes tlesize is 256x256'''
        data = utils.level_dic()  # our presets
        a, b, c, d = self.bbox
        r = 3
        dne = abs(round(float(c) - float(a), r))  # ne: North East point
        mylist = [round(i, r) for i in data.values()] + [dne]
        new = sorted(mylist, reverse=True)
        # print new, dne
        return new.index(dne)

    def read_image(self, im, ext='jpg'):
        '''Read img and return its contents'''
        if ext == 'jpg':
            ext = 'JPEG'
        elif ext == 'png':
            ext = 'PNG'
        buf = StringIO()
        #im2 = im.convert('RGB').convert('P', palette=Image.ADAPTIVE)
        im.save(buf, im.format, quality=self.get_quality(), optimize=True)
        data = buf.getvalue()
        return data, self.md5(data)

    def get_mask_color(self, src):
        '''Get mask attribute of a src in config file.'''
        mask_color_config = self.config.get('%s_mask_color' % src)
        if mask_color_config:
            return mask_color_config[0]  # So user sets the mask color ...

    def get_border(self, src):
        '''Get border attribute of a src in config file.'''
        border_config = self.config.get('%s_border' % src)
        condition = True
        if border_config:
            raw = border_config[0]  # So user sets the mask color ...
            name = raw
            if raw[0] == '-':
                name = raw[1:]
                condition = False
            return name, condition
        else:
            return None, True

    def sort_sections(self, a, b):
        '''resport sections. If border if available, they will be first items.'''
        digits = string.digits
        if self.config.get('%s_border' % a) and not self.config.get('%s_border' % b):
            return -1
        else:
            return 1

    def check_tile_in_disk(self):
        '''Check for cache in disk'''
        if self.get_cache_folder().lower() == 'memory':
            return
        compname = '{z}/{x}/{y}'.format(z=self.z, x=self.x, y=self.y)
        compath = self.get_fpath(
            compname + '.' + self.ext.replace('png8', 'png'), mode=2)
        if compath and os.path.isfile(compath) and not self.force:

            info = '200|{rid}|{layer}|Cache Hit|{z},{x},{y}|'\
                .format(rid=self.rid, layer=self.section, x=self.x,
                        y=self.y, z=self.z)
            logger.info(info)
            return open(compath, 'rb')

    def generate_wms_link(self, source=None, ext=None):
        '''Generate a link for fetching wms'''
        parameters = self.parameters
        ep = self.config.get('parameters')
        extra = ''
        if ep:
            extra = [map(str.strip, i.split(':')) for i in ep]
            if extra:
                extra = '&'.join(['='.join(i) for i in extra])
        #print extra
        if ext:
            parameters['FORMAT'] = 'image/%s' % ext
        if not source:
            url, parameters['LAYERS'] = utils.get_best_url(
                self.section).split('@')
        else:
            url, parameters['LAYERS'] = source.split('@')
        best = url
        iden = '?'
        if '?' in url:
            iden = '&'
        link = 'http://{url}{iden}{params}'\
            .format(url=best, iden=iden,
                    params=self.doseq(parameters))
        return link, best

    def generate_tms_link(self):
        '''Generate a link for fetching tms'''
        best = utils.get_best_url(self.section)
        url = best.format(z=self.z, x=self.x, y=self.y)
        link = 'http://{url}'.format(url=url)
        return link, best

    
    def generate_T_M_IR_link(self):
        '''reverse engeenering of map.tehran.ir'''
        #lat, lon = self.get_center_latlong()
        Z = self.z
        cof = 2**(Z-13)
        x = self.x-5262*cof
        y = 5*cof-(self.y-3223*cof)
        baseurl = 'http://map.tehran.ir/maptile/1.0.0/tehran_utm/{zoom}/{x}/{y}.png'
        final_link = baseurl.format(zoom=Z-12, x=x, y=y)
        #print final_link
        best = 'http://map.tehran.ir/maptile'
        return final_link, best


    def generate_here_link(self):
        '''Nokia Here API'''
        base = '{n}.aerial.maps.api.here.com'
        second = '/maptile/2.1/basetile/newest/{gmtype}/{z}/{x}/{y}/256/{ext}'
        bga = '?app_id=BL02UcAKiG3C4FMhfZYp&app_code=h5TRuNRPEn_Z4uwJPLAg-Q'
        self.loadbalance([base])
        best = utils.get_best_url(self.section)
        gmtype = self.config.get('map_type')
        ext = self.config.get('map_format')
        if gmtype:
            gmtype = gmtype[0]
        else:
            gmtype = 'satellite.day'
        if ext:
            ext = ext[0]
        else:
            ext = 'jpg'
        second = second.format(
            z=self.z, x=self.x, y=self.y, ext=ext, gmtype=gmtype)
        link = 'http://{url}'.format(url=best + second + bga)
        return link, best

    def generate_api_link(self, layertype):
        '''General method for google/bing APIs
        For bing Imagery type could be:
            One of the following values:
            Aerial – Aerial imagery.
            AerialWithLabels –Aerial imagery with a road overlay.
            Road – Roads without additional imagery.
            OrdnanceSurvey --Ordnance Survey imagery. This imagery is visible only for the London area.
            CollinsBart – Collins Bart imagery. This imagery is visible only for the London area.
        '''

        api_base = {
            'google_bga': 'http://maps.googleapis.com/maps/api/staticmap?',
            'bing_bga': 'http://dev.virtualearth.net/REST/v1/Imagery/Map',
            'mapquest_bga': 'http://www.mapquestapi.com/staticmap/v4/getmap?',
            'google_base': '{best}maptype={gmtype}&format={ext}&center={center}&zoom={zoom}&size=256x304&sensor=false&key=',
            'bing_base': '{best}/{gmtype}/{center}/{zoom}?format={ext}&mapSize=256,304&key=',
            'mapquest_base': '{best}center={center}&zoom={zoom}&size=256,304&type={gmtype}&imagetype={ext}&key=',
            'google_default_maptype': 'satellite', # could be roadmap, satellite, terrain, hybrid
            'bing_default_maptype': 'Aerial',
            'mapquest_default_maptype': 'sat',  # map,hyb,sat
            'bing_default_key': 'AkdrXqugT2553XscVA8i9mvl8Zw2p_uim2KWddUf8hvIumrIq-vKjgr4_2xohfEC',
            'google_default_key': 'AIzaSyBJeUbWxDjPiLqAiUv0uxKIBL4Tn03SanY',
            'mapquest_default_key': 'Fmjtd%7Cluur2q0b2h%2Crw%3Do5-9aaggy'}

        apx = self.config.get('key')
        gmtype = self.config.get('map_type')
        mformat = self.config.get('map_format')
        if apx:
            apx = apx[0]
        else:
            apx = api_base.get(layertype + '_default_key')
        if gmtype:
            gmtype = gmtype[0]
        if mformat:
            mformat = mformat[0]
        if not gmtype:
            gmtype = api_base.get(layertype + '_default_maptype')
        if not mformat:
            mformat = self.ext
        if mformat == 'jpg' and layertype in ['bing']:
            mformat = 'jpeg'
        best = api_base.get(layertype + '_bga')
        thekey = apx
        if '{' in apx and '}' in apx:
            apxpath = apx.split('{')[1].split('}')[0]\
                .replace('$data', utils.getpath('../data'))
            apikeys = map(str.strip, open(apxpath, 'r').readlines())
            apikeys = [i for i in apikeys if i]
            thekey = choice(apikeys)  # random distribusion
        center = ','.join(map(str, self.get_center_latlong()))
        base = api_base.get(layertype + '_base').format(best=best, zoom=self.z,
                                                        center=center, ext=mformat,
                                                        gmtype=gmtype)
        link = base + thekey
        return link, best

    def get_composite_layer(self):
        '''special composite layer'''

        ext = self.ext
        parameters = self.parameters
        sections = list()

        sections = ['src%s' %
                    i for i in xrange(16) if self.config.get('src%s' % i)]
        if self.config.get('mask'):
            sections += ['mask']  # mask if the first to check
        #sections = sorted(sections, cmp=self.sort_sections)
        images_data = dict()
        for section in sections:
            # print  'loading %s'%section
            if not section in sections:
                continue
            border, condition = self.get_border(section)
            if border:
                status = utils.is_tile_inside_border(
                    self.x, self.y, self.z, border)
                # print border, condition, status
                # 0: out
                # 1: in
                # 2: edge
                # 3: node
                if condition == True and (status == 0):  # outside
                    continue
                if condition == False and (status == 1):
                    continue

                if condition == True and (status == 1):  # inside
                    # remove others, just load this one
                    sections = [section]
                    # sections.extend([section])

                if condition == False and (status == 0):  # inside
                    # remove others, just load this one
                    sections = [section]
                    # sections.extend([section])

            internal_source_name = self.main_config.get(
                self.master).get(section)[0]
            self.section = internal_source_name
            self.config = self.main_config.get(internal_source_name)
            self.ext = self.master_ext
            if self.config.get('force_format'):
                self.ext = self.config.get('force_format')[0]
            # if self.config.get('force_format'):
                #self.ext = self.config.get('force_format')[0].lower()
            if section == 'mask':
                self.ext = 'png8'
            data, _ = self.go()
            if isinstance(data, file):
                images_data[section] = Image.open(StringIO(data.read()))
            elif isinstance(data, Image.Image):
                images_data[section] = data
            elif isinstance(data, str):
                return data
            self.section = self.master
            self.ext = self.master_ext
            self.config = self.main_config.get(self.section)
            if section == 'mask':  # force image format to be PNG
                needed_colors = utils.get_color_list(images_data[section])
                new_sections = list()
                for x in sections:
                    col = self.get_mask_color(x)
                    if col in needed_colors:
                        new_sections.append(x)
                sections = sorted(new_sections, reverse=True)
        mask_data = None
        if images_data.has_key('mask'):
            mask_data = images_data.pop('mask')

        if not mask_data and len(images_data.keys()):
            img = images_data.get(images_data.keys()[0])

        elif not len(images_data.keys()):
            return

        else:  # Ok we have mask and some layers to composite

            bg = Image.new('RGBA', (256, 256), (255, 255, 255, 0))

            for each in images_data.keys():
                fr = images_data.get(each)
                fr = fr.convert('RGB')
                mask_color = self.get_mask_color(each) or 'black'
                # So the mask color is really available
                if mask_color and mask_data:
                    bg = bg.convert('RGB')
                    mask = utils.mask(mask_data.convert('RGB'), mask_color)
                    # mask.show()
                    gc = greenlet(ImageChops.composite, parent=self.gr)
                    img = gc.switch(fr, bg, mask)
                    bg = img
            img = bg
        return img

    def loadbalance(self, urllist):
        '''Load balance urls ...'''
        if not urllist:
            return False
        for data_source in urllist:
            if '{n}' in data_source:
                for n in xrange(4):
                    src = data_source.replace('{n}', str(n + 1))
                    utils.addto_urls_database(self.section, src)
            elif '{c}' in data_source:
                for c in ['a', 'b', 'c', 'd']:
                    src = data_source.replace('{c}', c)
                    utils.addto_urls_database(self.section, src)
            else:
                utils.addto_urls_database(self.section, data_source)
        return True

    def getmaptype(self):
        #####################################
        layertype = 'tms'
        thetype = self.config.get('type')
        if thetype:
            layertype = thetype[0].lower()
        ############ Load Balance #############
        if layertype in ['wms', 'tms']:
            urllist = self.config.get('url')
            if not self.loadbalance(urllist):
                return
        return layertype

    def go(self):
        '''Find layer type, and route it to correct path'''
        available_tile = self.check_tile_in_disk()
        ext = self.ext
        ######### Check for available tiles ##
        if available_tile:
            return available_tile, self.cache_header

        layertype = self.getmaptype()
        if not layertype:
            return '405 Method Not Allowed , wms/tms type error in parsing CTC config!', 0
        ############## WMS #####################
        if layertype == 'wms':
            final_link, best = self.generate_wms_link(ext=ext)
        ############ TMS #######################
        elif layertype == 'tms':
            final_link, best = self.generate_tms_link()
        ############## BING / GOOGLE ###########
        elif layertype in ['google', 'bing', 'mapquest']:
            final_link, best = self.generate_api_link(layertype)
            pt = re.compile(r'key=([\w\d\-\+\%]+)')
            thekey = pt.findall(final_link)[0].strip()
            debug = '{rid}|{layertype}|{key}|{zoom},{x},{y}|'\
                .format(rid=self.rid, x=self.x, y=self.y, key=thekey,
                        zoom=self.z, layertype=layertype)
            apilog.debug(debug)
        ############## NOKIA HERE ###########
        elif layertype == 'here':
            final_link, best = self.generate_here_link()
        ##########################################
        elif layertype == 'tmri':  ## Tehran shahrdari map
            final_link, best = self.generate_T_M_IR_link()

        if layertype == 'composite':
            img = self.get_composite_layer()
        else:
            utils.update_urls_database(best, 'ADD')
            gs = greenlet(self.save, parent=self.gr)
            img = gs.switch(final_link, ext)
            if isinstance(img, Image.Image):
                utils.update_urls_database(best, 'REMOVE')

        if isinstance(img, Image.Image):
            if self.config.get('custom_text'):
                custom_text = self.config.get('custom_text')[0]
                if custom_text == 'pelak':
                    lat, lon = self.get_center_latlong()

                    x, y, _, _  = projections.to_utm(lat, lon)
                    data = utils.get_tehran_nosazi(x, y)
                    if data:
                        pelak = data.get('pelak')
                        nosazi = data.get('NosaziCode')
                        text = '%s' % (pelak)
                        img = utils.addtext(img, text, size=36, x=10, y=200, color=(107,196,237))
                        

            logofile = self.config.get('logo')
            logo_frequency = self.config.get('logo_frequency')
            if logo_frequency:
                logo_frequency = int(logo_frequency[0])
            else:
                logo_frequency = 2
            if logofile:
                logofile = logofile[0].replace(
                    '$data', utils.getpath('../data'))
                if os.path.isfile(logofile) and not (self.x + self.y) % logo_frequency:
                    img = utils.addlogo(img, logofile)
                elif not os.path.isfile(logofile):
                    logger.error('500|{rid}|{section}|File not found|{logofile}'
                                 .format(rid=self.rid, section=self.section, logofile=logofile))
            if self.get_cache_folder() != 'memory':
                return self.save_on_disk(img)
            else:
                return img, 0
        elif isinstance(img, str):
            return img, 0

    def save_on_disk(self, img):
        '''Save an image on disk and return file buffer/header'''
        if not isinstance(img, Image.Image):
            return 'ERROR 05', 0
        save_format = self.ext.upper()
        if self.ext.lower() == 'jpg':
            img = img.convert('RGB')
            save_format = 'JPEG'
        if self.ext.lower() == 'png8':
            save_format = 'PNG'
        # if self.ext == 'png':
            #img = img.convert('P')
        compname = '{z}/{x}/{y}'.format(z=self.z, x=self.x, y=self.y)
        addr = compname + '.' + self.ext.replace('png8', 'png')
        colors = utils.get_color_list(img, raw=True, ext=self.ext)
        if len(colors) == 1:
            color = colors[0]
            solidname = 'SOLID/%s.%s' % (color,
                                         self.ext.replace('png8', 'png'))

            fpath = self.get_fpath(addr, mode=2)
            solidpath = self.get_fpath(solidname, mode=2)
            # make sure file was not created before
            if not os.path.isfile(fpath):
                if not os.path.isfile(solidpath):
                    # save solid file again! this should never happen
                    img.save(
                        solidpath, save_format, quality=self.get_quality(),
                        optimize=True)
                try:
                    os.symlink('../../' + solidname, fpath)  # link it
                except OSError:
                    #error = '\t{t.red}[{rid}] Symlinks not supported!{t.normal}'.format(t=term, rid=self.rid)
                    img.save(fpath, save_format,
                             quality=self.get_quality(), optimize=True)

                debug = '200|{rid}|{layer}|Similarity Dtected'.format(
                    rid=self.rid, layer=self.section)
                logger.debug(debug)
                return open(fpath, 'rb'), self.cache_header
            else:
                ## this is when nothing is appropiate
                return img, 0

        fpath = self.get_fpath(addr, mode=2)

        if not os.path.isfile(fpath):
            if not os.path.isdir(os.path.dirname(fpath)):
                try:
                    os.makedirs(os.path.dirname(fpath))
                except OSError:
                    return '500, FILE ACCESS ERROR', 0
            img.save(fpath, save_format,
                     quality=self.get_quality(), optimize=True)
            center = self.get_center_latlong()
            localtime = time.localtime()
            lt = time.strftime("%Y-%m-%d %H:%M:%S", localtime)
            stat = os.stat(fpath)
            info = '200|{rid}|{layer}|Cache Saved|{lat}N,{long}E|{z},{x},{y}|{size}'\
                .format(rid=self.rid, size=stat.st_size, layer=self.section,
                        lat=round(center[0], 3), x=self.x, y=self.y,
                        z=self.z, long=round(center[1], 3))
            logger.info(info)

        return open(fpath, 'rb'), self.cache_header


class RequestTests(unittest.TestCase):

    def test1(self):
        return 1


if __name__ == '__main__':
    unittest.main()
