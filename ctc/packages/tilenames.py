#!/usr/bin/env python
# -*- coding: utf-8 -*-

_copyright='Chista Co.'
'''
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|
                                          
'''


import math
from math import *
from decimal import Decimal as d

def deg2num(lat_deg, lon_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
  return (xtile, ytile)



def num2deg(xtile, ytile, zoom):
  '''
  This returns the NW-corner of the square. 
  Use the function with xtile+1 and/or ytile+1 to get the other corners. 
  With xtile+0.5 & ytile+0.5 it will return the center of the tile.
  '''
  n = 2.0 ** zoom
  lon_deg = xtile / n * 360.0 - 180.0
  lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
  lat_deg = math.degrees(lat_rad)
  return (lat_deg, lon_deg)


def d_num2deg(xtile, ytile, zoom):
  '''
  This returns the NW-corner of the square. 
  Use the function with xtile+1 and/or ytile+1 to get the other corners. 
  With xtile+0.5 & ytile+0.5 it will return the center of the tile.
  '''
  xtile, ytile, zoom = d(xtile), d(ytile), d(zoom)
  n = d(2) ** zoom
  lon_deg = xtile / n * d(360) - d(180)
  lat_rad = math.atan(math.sinh(d(str(math.pi)) * (d(1) - d(2) * ytile / n)))
  lat_deg = math.degrees(lat_rad)
  return (float(lat_deg), float(lon_deg))


def numTiles(z):
  return(pow(2,z))

def sec(x):
  return(1/cos(x))

def latlon2relativeXY(lat,lon):
  x = (lon + 180) / 360
  y = (1 - log(tan(radians(lat)) + sec(radians(lat))) / pi) / 2
  return(x,y)

def latlon2xy(lat,lon,z):
  n = numTiles(z)
  x,y = latlon2relativeXY(lat,lon)
  return(n*x, n*y)
  
def tileXY(lat, lon, z):
  x,y = latlon2xy(lat,lon,z)
  return(int(x),int(y))

def xy2latlon(x,y,z):
  n = numTiles(z)
  relY = y / n
  lat = mercatorToLat(pi * (1 - 2 * relY))
  lon = -180.0 + 360.0 * x / n
  return(lat,lon)
  
def latEdges(y,z):
  n = numTiles(z)
  unit = 1 / n
  relY1 = y * unit
  relY2 = relY1 + unit
  lat1 = mercatorToLat(pi * (1 - 2 * relY1))
  lat2 = mercatorToLat(pi * (1 - 2 * relY2))
  return(lat1,lat2)

def lonEdges(x,z):
  n = numTiles(z)
  unit = 360 / n
  lon1 = -180 + x * unit
  lon2 = lon1 + unit
  return(lon1,lon2)



def tileEdges(x,y,z):
  lat1,lat2 = latEdges(y,z)
  lon1,lon2 = lonEdges(x,z)
  return((lat2, lon1, lat1, lon2)) # S,W,N,E

def mercatorToLat(mercatorY):
  return(degrees(atan(sinh(mercatorY))))

def tileSizePixels():
  return(256)


def level_dic():
    '''
    http://wiki.openstreetmap.org/wiki/Zoom_levels
    '''
    data = dict()
    for i in range(20):
        data[i] = 360.0 / (2 ** i)
    # print data
    return data

def getzoom(bbox):
    '''bbox is like x1,y1,x2,y2'''
    data = level_dic()  # our presets
    a, b, c, d = bbox
    r = 3
    dne = abs(round(float(c) - float(a), r))  # ne: North East point
    mylist = [round(i, r) for i in data.values()] + [dne]
    new = sorted(mylist, reverse=True)
    # print new, dne
    return new.index(dne)



if __name__ == '__main__':
    '''Run your tests here'''
    a = num2deg(85,53,7)
    b = d_num2deg(85,53,7)

    print a
    print b
