#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology! 
Clean code is much better than Cleaner comments!
'''


import sys
import os
import uuid
from blessings import Terminal
term = Terminal()
_version = '0.0.6'
if not sys.platform == 'linux2':
    print '{t.red}Your OS is {t.underline}not supported yet!{t.normal}'.format(t=term)
    sys.exit()


sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))

import argparse
from ctc.request import Request
from contenttype import contenttype
from cStringIO import StringIO
import os
from PIL import Image
import time
import getpass
import requests
import ctc.show as show
from cgi import parse_qs
from ctc.config import Config
import logging
from greenlet import greenlet


logger = logging.getLogger('ctc')
user = getpass.getuser()


class Main():

    def __init__(self):
        '''C'''
        self.conf = Config()
        self.config = self.conf.read()
        self.hello()
        self.session = requests.Session()

    def __del__(self):
        ''''''
        self.clean()

    def hello(self):
        '''Greetings'''
        # print term.clear
        print '\t{t.green}Hello {user}!{t.normal} Started on {date}'\
            .format(t=term, user=user.title(), date=time.ctime())

    def clean(self):
        '''Clean goodbye'''
        print term.clear_bol
        # print '\t' + term.cyan + ('-' * 22) + term.normal + ' Done ' +
        # term.cyan + ('-' * 22)
        print '\t{t.yellow}Closing the server{t.normal}'\
            .format(t=term)
        print '\t{t.green}Goodbye!{t.normal}'.format(t=term)
        print '\n'
        # print term.clear_eol

    def error(self, error):
        self.sr('400 Bad Request',
                [('Content-Type', 'application/json')])
        return (error)

    def commit(self):
        '''Save all dbs'''
        # self.db.commit()

    def __call__(self, environ, start_response):
        '''Getting the request form client. Parse it's contents
            and send it to the process
        '''
        # print environ
        rid = str(uuid.uuid4()).split('-')[0]
        ct = ('Content-type', 'image/png')
        srv = ('Server', 'Chista Backend Server | CTC')
        self.sr = start_response
        # print '\t' + term.cyan + ('-' * 50) + term.normal
        s = time.time()
        cpath = self.conf.get_cpath()
        # if os.stat(cpath).st_mtime != self.config['saved']:
            #self.config = self.conf.read()
        # print environ
        status = ('200 OK')
        #port = environ['SERVER_PORT']
        #server = environ['SERVER_NAME']

        # print 'PATH: ', environ.get('PATH_INFO')
        uri = environ.get('PATH_INFO').split('/')
        force = False  # By default, we're not forcing anything
        host = environ['HTTP_HOST']
        self.config['server'] = 'http://%s/%s' % (host, uri[1])
        gd = greenlet(Request(self.config, self.session, rid,
                              force).parse)

        if uri[1] == 'favicon.ico':
            return ''
        if uri[1] == 'tms':
            force = True
        if uri[1] == 'cachegraph':
            data = open('data/images/NC.png', 'rb')
            max_age = 0  # just 1 second and the expire
            layer, border, zoom, format = uri[2], uri[3], int(uri[4]), uri[5]
            text = None
            if len(uri) == 7:
                text = uri[6].strip()
            data = show.Show(layer, border, format).vis(zoom, user_defined_text=text,
                                        verbose=False)
            # if new_path:
            #data = open(new_path, 'rb')
        elif uri[1] in ['tms', 'static']:
            try:
                assert len(uri) >= 6
            except AssertionError:
                return self.error('Bad request. Must be /{layer}/{z}/{x}/{y}.{png|jpg}/text')

            #parameters = parse_qs(environ.get('QUERY_STRING', ''))
            data, max_age = 'Get Out', '1, must-revalidate'
            # if parameters.get('action'):
            #start_response('200 OK', [('Content-Type', 'application/json')])
            # return 'Done'
            #data, max_age = profile.runctx("req.go()", None, locals())
            import cProfile
            #data, max_age = cProfile.runctx("gd.switch(uri)", globals(), locals())
            data, max_age = gd.switch(uri)
            # handeling of static files
        else:
            data = '400 Bad Request, Getout!'
            max_age = 0

        info = '|{rid}|{section}|Response Time||||{time}|{ip}|{path}|'\
            .format(time=round(time.time() - s, 5), section=uri[2],
                    ip=environ.get('REMOTE_ADDR'), rid=rid, path=environ.get('PATH_INFO'))
        logger.debug(info)

        if isinstance(data, file):
            # if isinstance(data, io.BufferedIOBase):
            block_size = os.path.getsize(data.name)
            ct = ('Content-type', contenttype(data.name))
            response_headers = [
                ct, ('Content-Length', str(block_size), srv, 'Cache-Control: max-age=%s' % max_age)]
            start_response(status, response_headers)
            if 'wsgi.file_wrapper' in environ:
                return environ['wsgi.file_wrapper'](data, block_size)
            else:
                return iter(lambda: data.read(block_size), '')

        elif isinstance(data, Image.Image):
            buf = StringIO()
            data.save(buf, format='PNG')  # NOTE
            ct = ('Content-type', 'image/png')
            result = buf.getvalue()
            block_size = str(len(result))

            response_headers = [ct, ('Content-Length', block_size), srv]
            start_response(status, response_headers)
            return result
        elif isinstance(data, str):
            data = data.split(',')[:2] ## status , desc
            if len(data)>=2:
                status, desc = map(str.strip, data)
            else:
                status = data
                desc = "UNKNOWN ERROR"
            block_size = str(len(desc))
            ct = ('Content-type', 'text/html')
            ctcerror = ('ctc-error', desc)
            response_headers = [ct, ('Content-Length', block_size), srv, ctcerror]
            start_response(status, response_headers)
            return desc

