#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology!
Clean code is much better than Cleaner comments!
'''

import os
import sys
from PIL import Image  # for reading images.
from PIL import ImageChops  # for compositing borders.
from PIL import ImageFont
from PIL import ImageDraw
from PIL import ImageOps
# everything nedded to handle external commands
from subprocess import Popen, PIPE
from PIL import ImageMath
from PIL import ImageOps
from PIL import ImageFilter
import simplejson as sj
import packages.tilenames as t
import unittest
import time
import math
try:
    import uwsgi
except ImportError:
    pass
import array
import struct
import mmap
import tempfile
from random import shuffle
import cPickle as pickle
import functools
from glob import iglob
import _PointInPolygon as pip
import logging
import requests
from threading import Thread
from blessings import Terminal
term = Terminal()
usession = requests.Session()

from collections import defaultdict
from collections import OrderedDict

FILE = __file__


def decorator(d):
    """Make function d a decorator: d wraps a function fn.
     Peter Norvig, my good friend at Dropbox"""
    def _d(fn):
        return functools.update_wrapper(d(fn), fn)
    functools.update_wrapper(_d, d)
    return _d


@decorator
def Memoized(func):
    """Decorator that caches a function's return value  PS: Results
      This function is the reason I love python.
      without cache: 7.6e-06
      with cache: 3.2e-07
    """
    cache = {}
    key = (func.__module__, func.__name__)
    # print key
    if key not in cache:
        cache[key] = {}
    mycache = cache[key]

    def _f(*args):
        try:
            return mycache[args]
        except KeyError:
            value = func(*args)
            mycache[args] = value
            return value
        except TypeError:
            return func(*args)

    _f.cache = cache
    return _f


class external_fetch(Thread):

    def __init__(self, url):
        Thread.__init__(self)
        self.url = url
        print url

    def run(self):
        r = usession.head(self.url)
        if r.status_code == 200:
            return True
        # return r.content


def setup_logger(logger_name, log_file, level=logging.DEBUG):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter(
        '%(asctime)s|%(levelname)s|%(module)s|%(message)s')
    fileHandler = logging.FileHandler(log_file, mode='a')
    fileHandler.setFormatter(formatter)
    #streamHandler = logging.StreamHandler()
    # streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)

    # l.addHandler(streamHandler)
    return l


@Memoized
def get_response(code):
    responses = {
        200: ('OK', 'Request fulfilled, document follows'),
        201: ('Created', 'Document created, URL follows'),
        202: ('Accepted',
              'Request accepted, processing continues off-line'),
        203: ('Partial information', 'Request fulfilled from cache'),
        204: ('No response', 'Request fulfilled, nothing follows'),

        301: ('Moved', 'Object moved permanently -- see URI list'),
        302: ('Found', 'Object moved temporarily -- see URI list'),
        303: ('Method', 'Object moved -- see Method and URL list'),
        304: ('Not modified',
              'Document has not changed singe given time'),

        400: ('Bad request',
              'Bad request syntax or unsupported method'),
        401: ('Unauthorized',
              'No permission -- see authorization schemes'),
        402: ('Payment required',
              'No payment -- see charging schemes'),
        403: ('Forbidden',
              'Request forbidden -- authorization will not help'),
        404: ('Not found', 'Nothing matches the given URI'),

        500: ('Internal Server Error', 'Server got itself in trouble'),
        501: ('Not implemented',
              'Server does not support this operation'),
        502: ('Gateway timeout',
              'The server cannot process the request due to a high load'),
        503: ('Gateway timeout',
              'Unable to handle the HTTP request due to a temporary overloading or maintenance of the server'),
        504: ('Gateway timeout',
              'The upstream server is down (no response to the gateway/proxy)'),
    }
    return responses.get(code)


def mapfile(filename):
    '''
    @param filename: a path to a file
    @return: mmap
    @usage:
        data = mapfile("samples/sample.txt")
    '''
    f = open(filename, "rb+")
    size = os.path.getsize(filename)
    return mmap.mmap(f.fileno(), size)


def process(cmd):
    '''General external process'''
    p = Popen(cmd, shell=True, stderr=PIPE, stdout=PIPE,
              )
    # universal_newlines=True)  # process
    (stdout, stderr) = p.communicate()
    return (stdout, stderr)


@Memoized
def getpath(name):
    dir = os.path.abspath(os.path.join(os.path.dirname(FILE), name))
    # if not (os.path.isdir(dir) or os.path.isfile(dir)):
    # os.makedirs(dir)
    return dir


@Memoized
def get_seed_tiles(border, zoom):
    tiles = list()
    pick_file = getpath('../data/cache/%s-%s.pick' % (border, zoom))
    if os.path.isfile(pick_file):
        pick = open(pick_file, 'rb')
        tiles = pickle.load(pick)
        return tiles


def get_available_tiles(cachedir, zoom, x='*', ext='*'):
    '''return a list list [(x1,y1), ...]'''
    files = iglob('%s/%s/%s/*.%s' % (cachedir, zoom, x, ext))
    tiles = ((int(i.split('/')[-2]), int(i.split('/')[-1].split('.')[0]))
             for i in files)
    return tiles






def get_salt():
    return "Oh no no no!!, please! don't kill my k@ farsheed!"


def get_pal(im):
    ''' return palate data of an image'''
    p = im.convert('P')
    data = [i[1] for i in p.getcolors()]
    return data


def get_color_list(im, raw=None, ext='png'):
    '''Gives an Image and returns colors in it'''
    MAX = 4  # 5x5 pixels
    preset = {
        '255-0-0': 'red',
        '0-0-0': 'black',
        '255-0-255': 'magenta',
        '0-0-255': 'blue',
        '0-255-0': 'green',
        '0-255-255': 'green',
        '255-255-255': 'white',
    }
    if ext == 'png':
        im = im.convert("P")
    im = im.convert('RGB')
    pt = im.getcolors()
    #
    if not raw and pt:

        data = ('%s-%s-%s' % (i[1][0], i[1][1], i[1][2])
                for i in pt if i[0] > MAX)
        # print data
        return [preset.get(i) for i in data if i in preset]
    elif pt:
        return ['%s-%s-%s' % (i[1][0], i[1][1], i[1][2]) for i in pt]
        # print p.getcolors()
        # return [''i[1] for i in p.getcolors()]
    else:
        return list()


def composite(bg, fr, mask, output='over.jpg'):
    '''composite two images based on a black&white mask.
    @param bg: string path to background image.
    @param fr: string path to forground image.
    @param mask: string path to mask image.
    @param output: string path to output image.
    '''
    im1 = Image.open(fr)
    im2 = Image.open(bg)
    border = Image.open(mask)
    mask = border.split()[-1]
    x = ImageChops.composite(im1, im2, mask)
    x.save(output)
    return output


@Memoized
def level_dic():
    '''
    http://wiki.openstreetmap.org/wiki/Zoom_levels
    '''
    data = {0: 360.0,
            1: 180.0,
            2: 90.0,
            3: 45.0,
            4: 22.5,
            5: 11.25,
            6: 5.625,
            7: 2.813,
            8: 1.406,
            9: 0.703,
            10: 0.352,
            11: 0.176,
            12: 0.088,
            13: 0.044,
            14: 0.022,
            15: 0.011,
            16: 0.005,
            17: 0.003,
            18: 0.001,
            19: 0.0005}
    return data


def addto_urls_database(name, baseurl):
    '''Add a url to internal uwsgi urls memory database'''
    data = uwsgi.cache_get(name)
    if not data:
        #utils.urls_database[name] = [baseurl]
        uwsgi.cache_set(name, baseurl)

    oldies = uwsgi.cache_get(name).split(',')
    if not baseurl in oldies:
        uwsgi.cache_update(name, data + ',' + baseurl)

    # Update details
    udata = uwsgi.cache_get(baseurl)
    if not udata:
        uwsgi.cache_set(baseurl, '0')


def update_urls_database(baseurl, mode):
    '''updates uwsgi urls memory database'''
    data = uwsgi.cache_get(baseurl)
    if data:
        count = int(data)

        if mode == 'ADD':
            # print 'MODE:Add, url %s, count %s' % (baseurl, count)
            uwsgi.cache_update(baseurl, str(count + 1))

        elif mode == 'REMOVE':
            uwsgi.cache_update(baseurl, str(count - 1))

    else:
        uwsgi.cache_set(baseurl, '0')


def sort_urls(item1, item2):

    if item1[1] > item2[1]:
        return 1
    else:
        return -1


def get_best_url(name):
    availables = uwsgi.cache_get(name).split(',')
    lol = [[i, uwsgi.cache_get(i)] for i in availables]
    shuffle(lol)
    lol = sorted(lol, cmp=sort_urls)
    # print lol
    dest = lol[0]
    # because i want to randomly select ZERO sources, lets shuffle
    shuffle(lol)
    for i in lol:
        # print i[1][0]
        if i[1] == '0':
            dest = i
    return dest[0]


def crop_to_256(img):
    x, y = img.size
    topx = (x - 256) / 2
    topy = (y - 256) / 2
    btx = x - topx
    bty = y - topy
    box = (topx, topy, btx, bty)
    return img.crop(box)


def addtext(img, text, size=16, x=60, y=150, color=(51, 51, 51)):
    text = str(text)
    draw = ImageDraw.Draw(img)
    font = getpath("../data/fonts/sofiaprolight.ttf")
    font = ImageFont.truetype(font, size)
    draw.text((x, y), text, color, font=font)
    return img


def chroma(img, col='green'):
    '''tun main key to alpha'''
    d = { 'red':0, 'green':1, 'blue':2 }
    test = img.split()[d.get(col, 1)]
    alp = ImageOps.invert(test)
    img.putalpha(alp)
    return img


def addlogo(img, logofile):
    '''Add a logo to a tile image'''
    w, h = img.size
    logo = Image.open(logofile)
    lw, lh = logo.size
    offset = (w - lw - 20, h - lh - 20)
    foreground = Image.new('RGBA', (w, h))
    foreground.paste(logo, offset)
    mask = foreground.split()[3]
    mask = ImageOps.invert(mask)
    img = Image.composite(img, foreground, mask)
    return img.convert('RGB')


@Memoized
def get_color(color='white'):
    MAX = 255
    MIN = 0
    if color == 'black':
        return (MIN, MIN, MIN)
    if color == 'white':
        return (MAX, MAX, MAX)
    if color == 'red':
        return (MAX, MIN, MIN)
    if color == 'green':
        return (MIN, MAX, MIN)
    if color == 'blue':
        return (MIN, MIN, MAX)
    if color == 'magenta':
        return (MAX, MIN, MAX)
    if color == 'yellow':
        return (MAX, MAX, MIN)
    if color == 'cyan':
        return (MIN, MAX, MAX)
    else:
        return (MAX, MAX, MAX)


@Memoized
def difference1(source, color):
    """When source is bigger than color"""
    return ((255.0 - color) / source - color)


@Memoized
def difference2(source, color):
    """When color is bigger than source"""
    return color / (color - source)


@Memoized
def color_to_L(image, color=None):
    width, height = image.size
    color = map(float, color)
    img_bands = [band.convert("F") for band in image.split()]
    alpha = ImageMath.eval(
        """float(
            max(
                max(
                    max(
                        difference1(red_band, cred_band),
                        difference1(green_band, cgreen_band)
                    ),
                    difference1(blue_band, cblue_band)
                ),
                max(
                    max(
                        difference2(red_band, cred_band),
                        difference2(green_band, cgreen_band)
                    ),
                    difference2(blue_band, cblue_band)
                )
            )
        )""",
        difference1=difference1,
        difference2=difference2,
        red_band=img_bands[0],
        green_band=img_bands[1],
        blue_band=img_bands[2],
        cred_band=color[0],
        cgreen_band=color[1],
        cblue_band=color[2]
    )

    a = ImageMath.eval("convert(255 * alpha, 'L')", alpha=alpha)
    new_bands = [a]

    result = Image.merge('L', new_bands)
    data = result.filter(ImageFilter.MedianFilter(size=9))
    #data = result
    data = ImageOps.invert(data)
    # data.show()
    return data


@Memoized
def mask(im, color='red'):
    w, h = im.size
    color = get_color(color)
    image = color_to_L(im, color)
    return image


@Memoized
def get_data(name):
    BF = getpath('../data/borders/%s.txt' % name)
    try:
        return open(BF, 'r').readlines()
    except IOError:
        print '\tO ohhh! Please copy %s border to data/borders/%s.txt' % (name, name)
        sys.exit()


@Memoized
def deg2numlist(input_list):
    '''input [['lat,lon,z'],...]'''
    lat, lon, z = input_list
    return t.deg2num(lat, lon, z)


@Memoized
def get_deg2num(z, data):
    '''convert each line of a lat,long list file to tiles'''
    data = (line.split(',') + [z] for line in data)
    data = (map(float, s) for s in data)
    return map(deg2numlist, data)


@Memoized
def raster(poly):
    s = time.time()
    for index in range(len(poly) - 1):
        t1 = poly[index]
        t2 = poly[index + 1]
        xdef = t1[0] - t2[0]
        ydef = t1[1] - t2[1]
        if abs(xdef) > 1:
            lo = min(t1[0], t2[0])
            hi = max(t1[0], t2[0])
            j = lo
            while(j <= hi):
                new = (j, t1[1])
                poly.append(new)
                j += 1
        if abs(ydef) > 1:
            lo = min(t1[1], t2[1])
            hi = max(t1[1], t2[1])
            j = lo
            while(j <= hi):
                new = (t1[0], j)
                poly.append(new)
                j += 1
    return poly


def sh(poly, X, Y, minx, miny):
    te = Image.new("RGB", (X, Y))
    draw = ImageDraw.Draw(te)
    new = list()
    for xy in poly:

        new.append((xy[0] - minx, xy[1] - miny))
    for xy in new:
        # print xy
        draw.point(xy, fill=(158, 183, 223))
    te = te.resize((640, 640 * Y / X))
    te.show()
    #os.system("sleep 5;pkill display;")


def pra(x, y):
    if x[0] >= y[0]:
        return -1
    else:
        return 0


def get_tehran_nosazi(x, y):
    '''Get Pelak from http://map.tehran.ir/services/nosazi
        x, y are in UTM
    '''
    base = 'http://map.tehran.ir/services/nosazi/search.php?x={x}&y={y}'
    url = base.format(x=x, y=y)
    response = requests.get(url)
    print url
    if response.status_code==200:
        content = response.content
        print content
        if content !='null':
            data =  sj.loads(response.content)
            return data



@Memoized
def getbbox(poly):
    '''
    input is (lat,long) points in a list
    output is standard minx, miny, maxx, maxy    
    >>> getbbox([(35.85,51.7),(35.5,51.05)])
    (35.5, 51.05, 35.85, 51.7)
    '''
    xs, ys = zip(*poly)
    minx, maxx, miny, maxy = min(xs), max(xs), min(ys), max(ys)
    return (minx, miny, maxx, maxy)


@Memoized
def render(poly, z, show=False, border=None):
    if z < 9:
        print '\t{t.red}Error: Z must be greater than 8{t.normal}'.format(t=term)
        return list()  # Must be greater than 8 or empty
    pick_dir = getpath('../data/cache')
    if not os.path.isdir(pick_dir):
        os.makedirs(pick_dir)
    #pick_file = ''
    if border:
        pick_file = getpath('../data/cache/%s-%s.pick' % (border, z))
        if os.path.isfile(pick_file):
            pick = open(pick_file, 'rb')
            tiles = pickle.load(pick)
            if tiles:
                return tiles

    xs = [i[0] for i in poly]
    ys = [i[1] for i in poly]
    minx, maxx = min(xs), max(xs)
    miny, maxy = min(ys), max(ys)
    X = maxx - minx + 1
    Y = maxy - miny + 1
    newPoly = [(x - minx, y - miny) for (x, y) in poly]
    i = Image.new("RGB", (X, Y))
    draw = ImageDraw.Draw(i)
    draw.polygon(newPoly, fill="red")
    # i.show()

    tiles = set()
    w, h = i.size
    for x in range(w):
        for y in range(h):
            data = i.getpixel((x, y))
            if data != (0, 0, 0):
                tiles.add((x + minx, y + miny))
    if show:
        sh(tiles, X, Y, minx, miny)

    tiles = tiles.union(set(poly))  # + border
    tiles = list(set(tiles))
    # random.shuffle(tiles)
    if border:
        pick = open(pick_file, 'wb')
        pickle.dump(tiles, pick)
    return tiles


@Memoized
def bboxtopoly(name, minx, miny, maxx, maxy):
    '''
        generates standard counter-clockwise order of a polygon
        >>> f = bboxtopoly('testme',35.05,51.05, 35.5,51.7)
        >>> print open(f, 'r').read()
        51.7,35.05
        51.05,35.05
        51.05,35.5
        51.7,35.5
    '''
    nw = str(maxy), str(minx)
    ne = str(maxy), str(maxx)
    sw = str(miny), str(minx)
    se = str(miny), str(maxx)
    result = '%s,%s\n%s,%s\n%s,%s\n%s,%s' % (nw[0].strip(), nw[1].strip(), sw[0].strip(
    ), sw[1].strip(), se[0].strip(), se[1].strip(), ne[0].strip(), ne[1].strip())
    fp = getpath('../data/borders/%s.txt' % name)
    with open(fp, 'w') as f:
        f.write(result)

    # return counter-clock
    return fp


@Memoized
def polygon_resort(poly):
    '''sorts your points according to polar coordinates
    >>> polygon_resort([(1,2), (3,5), (1,1), (54,45), (0,0)])
    [(3, 5), (1, 2), (1, 1), (0, 0), (54, 45)]
    '''

    cent = (sum([p[0] for p in poly]) / len(poly), sum([p[1]
                                                        for p in poly]) / len(poly))
# sort by polar angle
    poly.sort(key=lambda p: math.atan2(p[1] - cent[1], p[0] - cent[0]))
    return poly


def tiles_to_binary_file(tiles_list):
    '''Write tile points in a file in binary format'''
    tiles_file_bin = tempfile.NamedTemporaryFile(
        'wb', delete=False)
    for tile in tiles_list:
        # write out binary file
        binary = array.array('i', [tile[0], tile[1]])
        binary.tofile(tiles_file_bin.file)
    tiles_file_bin.close()
    return tiles_file_bin.name


def read_binary_tiles(filepath):
    '''Read tiles from a binary file'''
    tiles = list()
    tiles_file = open(filepath, 'rb')
    tno = os.path.getsize(filepath) / 8
    for line in range(tno):
        tiles.append(
            struct.unpack('2i', tiles_file.read(8)))
    return tiles


class bcolors:
    HEADER = '\033[95m \t'
    OKBLUE = '\033[94m \t'
    OKGREEN = '\033[92m \t'
    WARNING = '\033[93m \t'
    FAIL = '\033[91m \t'
    ENDC = '\033[0m \t'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


@Memoized
def get_binary(tiles):
    '''Return a file object'''
    tiles = list(OrderedDict.fromkeys(tiles))  # remove dups
    border_file_bin = tempfile.NamedTemporaryFile('wb', delete=False)
    arrayfunc = array.array
    for i in tiles:
        binary = arrayfunc('i', [i[0], i[1]])  # write out binary file
        binary.tofile(border_file_bin.file)
    border_file_bin.close()
    return border_file_bin.name


@Memoized
def get_border_binary(border, zoom):
    '''returns a string path to binary file'''
    bpath = getpath('../data/borders/%s.txt') % border
    if not os.path.isfile(bpath):
        raise(IOError, 'Not a border text file!')
    data = open(bpath, 'r').readlines()
    border_tiles = get_deg2num(zoom, data)
    return get_binary(border_tiles)


@Memoized
def isInside(tile, border_binary):
    '''Checkes if tile is inside border'''
    status = pip.check_point(tile[0], tile[1], border_binary)
    return status


@Memoized
def mathme(x):
    o = list()
    for i in xrange(100000):
        o.append(math.cos(x))
    return o


@Memoized
def is_tile_inside_border(x, y, z, border):
    '''Checks if a tile is inside border or not
    >>> is_tile_inside_border(85,53,7,'iran')
    3
    >>> is_tile_inside_border(85,15,7,'iran')
    0
    '''
    binary_data = get_border_binary(border, z)
    return isInside((x, y), binary_data)


def goo():
    border = 'iran'
    z = 15
    data = open(getpath('../data/borders/%s.txt') % border, 'r').readlines()
    border_tiles = get_deg2num(z, data)
    border_file_bin = get_binary(border_tiles)
    minx, miny, maxx, maxy = getbbox(border_tiles)
    # polygon = ' '.join([' '.join([str(i[0]), str(i[1])])
    #                   for i in border_tiles])
    # print polygon
    X = (maxx - minx) + 1
    Y = (maxy - miny) + 1
    total = X * Y
    print 'generating %s tiles ...' % (total)
    a, b = minx, miny
    result = set()
    #result = set(border_tiles)
    count = 0
    tiles_file_bin = tempfile.NamedTemporaryFile('wb', delete=False)

    while (minx <= maxx):
        while (miny <= maxy):
            # pip.check_point(minx,miny,border_file_bin.name)

            binary = array.array('i', [minx, miny])  # write out binary file
            binary.tofile(tiles_file_bin.file)
            miny += 1
            count += 1
        miny = b
        minx += 1
        # print point
    tiles_file_bin.close()
    #result = sorted(list(result))
    # Now, Lets call our C module
    verified_tiles_file = tempfile.NamedTemporaryFile('r', delete=True)
    s = time.time()
    # print pip.check_point(85,53,border_file_bin.name)
    # sys.exit()
    if not pip.bulk_check(border_file_bin, tiles_file_bin.name, verified_tiles_file.name):
        print "Error!"
        sys.exit()
    print 'C extension time:', (time.time() - s)

    tno = os.path.getsize(verified_tiles_file.name) / 8
    result = list()
    for i in range(tno):
        # pass
        result.append(struct.unpack('2i', verified_tiles_file.read(8)))

    sh(result, X, Y, a, b)

    # cleanup:
    for i in (tiles_file_bin.name, border_file_bin):
        if os.path.isfile(i):
            os.remove(i)

    s = time.time()
    data = get_data(border)
    polygons = get_deg2num(z, data)
    render(polygons, z, border=border, show=True)
    print 'PIL time: %s' % (time.time() - s)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    #s = time.time()
    # for i in range(100):
        #res = mathme(100)
    # print time.time()-s
