#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology!
Clean code is much better than Cleaner comments!
'''


'''
@desc: seed.py
@c: Chista Co
@author: F.Ashouri
@version: 0.1.8
'''


import os
import sys
if 'threading' in sys.modules:
    raise Exception('threading module loaded before patching!')
import gevent.monkey
gevent.monkey.patch_all()
import multiprocessing
from gevent.pool import Pool
from gevent import Timeout
import gevent
import requests
#import grequests
from functools import partial
import time
import datetime
import utils
import uuid
import array
import struct
import show
import tempfile
import config
import _PointInPolygon as pip
import argparse
import logging
from greenlet import greenlet
#from time import strftime
from blessings import Terminal
term = Terminal()

############ Logging #########################
ctcroot = os.path.abspath(os.path.dirname((os.path.join(__file__, '../../'))))
localtime = time.localtime()
lt = time.strftime("%Y-%m-%d-%H-%M-%S", localtime)
logger = logging.getLogger(__name__)
hdlr = logging.FileHandler('%s/logs/seed-%s.log' % (ctcroot, lt))
formatter = logging.Formatter(
    '%(asctime)s|%(levelname)s|%(module)s|%(message)s')
hdlr.setFormatter(formatter)
if not logger.handlers:
    logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)
############ Logging #########################

Config = config.Config().read()
#from gevent.queue import Queue
tasks = multiprocessing.Queue()
NUMBER_OF_PROCESSES = max(1, multiprocessing.cpu_count() - 1)
#NUMBER_OF_PROCESSES = 1
now = time.time
start = now()
gid = str(uuid.uuid4()).split('-')[0]

def Mcounter():
    x = 0
    while True:
        x += 1
        yield str(x).zfill(5)


MC = Mcounter()


def kill_me(message=False):
    '''
    Termination module
    '''
    seed_pid_file = utils.getpath("../logs/seed.pid")
    mypid = os.getpid()
    ############## delete extra png graph files ###################
    image_directory = utils.getpath('../app/static/img')
    # clean ups
    delete_temp_png_graphs_cmd = 'rm -rf %s/*.png' % (image_directory)
    utils.process(delete_temp_png_graphs_cmd)
    #############################################################
    if os.path.isfile(seed_pid_file):
        with open(seed_pid_file, 'r') as pf:
            pids = [p.strip() for p in sorted(pf.readlines(), reverse=True)]
            logger.debug('Shutting down')
            cmd = 'kill %s' % ' '.join(pids)

        print >> sys.stderr, '\tKilling processes ...',
        os.remove(seed_pid_file)
        se, so = utils.process(cmd)
        if se:
            print se
        print >> sys.stderr, '\tDone.'
    elif message:
        print '\tNo Active Seed Processes Found.'


def get_similar_layer(name):
    '''find similar layers and return it as a list'''
    result = list()
    sections = map(str.lower, Config.keys())
    if name.lower() in sections:
        fno = sections.index(name.lower())
        sections = Config.keys()
        newname = sections[fno]
        result.append(newname)
    return result


def make_gif(args, zoom, border):
    ''' Create show graphs of tiles, save them and
        make a gif animation out of them. ;)
    '''
    time.sleep(1)
    dirname = os.path.dirname
    image_directory = utils.getpath('../app/static/img/graphs')
    lst = []
    def convert_to_gif():
        lst.append(1)
        output_gif_animated_graph_path = '%s/%s-%s-%s.gif' % (image_directory,gid,
                                                           border, zoom)
        convert_to_gif_command = 'convert -delay %s -loop 0 "%s/%s-%s-*.png" "%s"' % (args.graph_duration[0],
                    image_directory, gid, border, output_gif_animated_graph_path)
        utils.process(convert_to_gif_command)
        if len(lst) == 1:  ## print once
            print '\n\tSeed graph: http://127.0.0.1:8085/app/static/img/graphs/%s-%s-%s.gif \n' % (gid,border, zoom)

    while True:
        delta = int(now() - start)
        if (not delta % args.graph_frequency[0]):

            graph_image_path = utils.getpath(
                '../app/static/img/graphs/%s-%s-%s.png' % (gid, border, MC.next()))
            s = show.Show(args.layer, border, ext=args.image_format)
            vis = s.vis(zoom, 'Seed Graph: %s on %s layer' %
                        (border, args.layer), verbose=False)
            vis.save(graph_image_path)

            convert_to_gif()
        time.sleep(0.5)




class Seed(object):

    def __init__(self, args, test=False):
        '''Nothing'''
        self.layer = args.layer
        self.counter = 0
        self.args = args
        self.failed_urls = set()
        self.attemps = 0
        self.start_time = time.time()
        self.session = requests.Session()
        self.tps_timer = self.start_time
        self.base = 'http://{ip}:{port}/{part}/{layer}/{z}/{x}/{y}.{ext}'
        self.config = Config.get(self.layer)
        cachedir = self.config.get('cache')
        if isinstance(cachedir, list):
            cachedir = cachedir[0]
        if not (self.config.get('cache_exact_path') and self.config.get('cache_exact_path')[0] == 'true'):
            self.cache_folder = cachedir + os.path.sep + self.layer
        else:
            self.cache_folder = cachedir
        if not test and not self.config:
            error = '500||{section}|Wrong layer!||||'.format(
                section=self.layer)
            print '\tWrong Layer! (Concider that names are Case Senstive!)'
            print '\tSearching for a similar name ...'
            simils = get_similar_layer(self.layer)
            if simils:
                print '\tI found similar layers: [%s] | Please fix the command' % ','.join(simils)
            logger.error(error)
            sys.exit()
        self.itsok = True

    def __del__(self):
        '''cleanup'''
        # self.db._adapter.close_all_instances(None)

    def fetch(self, url, timeout, saveit=False, save_urls_in_file=None):
        # print self.pool.free_count(), self.pool.size
        try:
            response = self.session.get(url, timeout=timeout, stream=False)
            elapsed = response.elapsed.microseconds / 1000000.0
            server_name = response.headers.get('server')
            reason = response.reason
            ctcerror = response.headers.get('ctc-error') or 'OverLoaded NGINX/CTC!'

            if 'ctc' in server_name.lower():
                server_name = 'CTC SERVER'
            else:
                server_name = 'STATIC SERVER'
            if response.status_code == 200:
                self.counter += 1
                info = '200|{reason}|Cache Saved|{url}|{el}|{server}'\
                    .format(reason=reason, el=elapsed, 
                            url=url, server=server_name)
                logger.debug(info)
                print >> sys.stderr, '.',
            else:
                error = '{st}|{reason}|{ctcerror}|NOT Saved|{url} |{el}'\
                    .format(st=response.status_code, reason=reason,
                            el=elapsed, url=url, ctcerror=ctcerror)
                logger.error(error)
                self.failed_urls.add(url)
                print >> sys.stderr, '?',
            del response


        except requests.Timeout:
            warning = '504|Gateway Timeout|NOT Saved|{url}'\
                    .format(url=url)
            logger.warning(warning)
            self.failed_urls.add(url)
            #failed.put(url)
            print >> sys.stderr, 'T',

        except requests.RequestException:
            critical = '500|Internal Server Error|An Error in connecting or fetching!|NOT Saved|{url}'\
                    .format(url=url)
            logger.critical(critical)
            self.failed_urls.add(url)
            #failed.put(url)
            print >> sys.stderr, 'E',

        except Exception, e:
            error = e
            logger.error(error)
            self.failed_urls.add(url)
            #failed.put(url)
            print >> sys.stderr, 'E',
            pass
            # self.pool.spawn(self.fetch, url, timeout) ##add it again
        # print "Status: [%s] URL: %s" % (response.status_code, url)
        # print self.attemps
        except KeyboardInterrupt:
            print '\n\tShutting down during fetch process! Wait ...\n'
            print '\tGoodbye.'
            kill_me()
            sys.exit()
        self.attemps += 1
        # not bigger than 128
        cc = min(128, self.args.concurrent_download_size[0])
        if not self.attemps % 100:
            print '\n'
            localtime = time.localtime()
            lt = time.strftime("%Y-%m-%d %H:%M:%S", localtime)
            info = 'Seeded %s urls with %s percent success using %s threads. [ %s TPS] | Tasks: %s' % (self.attemps,
                            round( (self.counter * 100.0) / self.attemps, 3), cc,
                            round(100 / (time.time() - self.tps_timer), 2), tasks.qsize())

            print >> sys.stderr, '\t', info, '\n'
            logger.info(info)
            self.tps_timer = time.time()

    def tile_generator(self, level, host, ps, lock):
        '''Yields tiles'''
        try:
            counter = 0
            minx, miny, maxx, maxy = self.bbox
            X = (maxx - minx) + 1
            Y = (maxy - miny) + 1
            mypart = X / ps
            remaining = X % ps
            start, end = host * mypart, (host + 1) * mypart
            # Lets put the burden of rremainings to last host:
            if host == ps - 1:  # last host:
                end = end + remaining
            for i in xrange(start, end):
                tiles = list()
                points = list()
                b = miny
                avail = utils.get_available_tiles(
                    self.cache_folder, level, x=(i + minx),
                    ext=self.args.image_format)
                while (b <= maxy):
                    point = (i + minx, b)
                    counter += 1
                    if not point in avail:
                        points.append(point)
                    b += 1  # walk along

                if not points:
                    continue
                verified_tiles_file = tempfile.NamedTemporaryFile(
                    'r', delete=True)
                tiles_bin = utils.tiles_to_binary_file(points)
                if not pip.bulk_check(self.border_file_bin, tiles_bin,
                                      verified_tiles_file.name):
                    print "Error!"
                    sys.exit()
                tiles = utils.read_binary_tiles(verified_tiles_file.name)
                lock.acquire()
                print >> sys.stderr, ' |', len(
                    points), '->', len(tiles), '|\t',
                lock.release()
                for tile in tiles:

                    tasks.put(tile)
                # yield tiles
                gevent.sleep(0)
            tasks.put('STOPSEED_%s' % host)

        except KeyboardInterrupt:
            kill_me()
            sys.exit()
        # print 'x:%s, y:%s, counter:%s, host:%s, start:%s, end:%s, ps:%s' % (X,
        #                Y, counter, host, start, end, ps)

    def get_url(self, tile):
        '''Generate a url'''
        args = self.args
        return self.base.format(
            ip=args.url,
            port=args.port,
            part='static',
            layer=self.layer,
            z=tile[2],
            x=tile[0],
            y=tile[1],
            ext=args.image_format)

    def start_fetch_pool(self):
        #import packages.tilenames as tn
        stop = 0
        while True:
            try:
                if not tasks.empty():
                    tile = tasks.get()
                    if 'STOPSEED' in tile:
                        stop += 1
                        if stop == NUMBER_OF_PROCESSES:
                            break
                        else:
                            continue
                        # self.start_fetch_pool()
                    x, y = tile
                    url = self.get_url((x, y, self.level))
                    #lat, lon = tn.num2deg(x, y, self.level)
                    ## Lat lon generator
                    #with open('saved_tiles_tehran', 'a') as f:
                        #f.write('%s,%s\n'% (lat, lon))

                    self.pool.spawn(self.fetch, url, self.args.timeout[0])
                elif tasks.qsize() < 1:
                    gevent.sleep(0.5)

            except KeyboardInterrupt:
                kill_me()
                sys.exit()

        self.pool.join()  # Wait for all

    def start_seed(self, border, level):
        #self.session = requests.Session()
        # not bigger than 128
        cc = min(128, self.args.concurrent_download_size[0])
        self.pool = Pool(cc)
        print '\n\tStart to seed on border %s level %s\n' % (border, level)
        info = 'Seeding border:%s, level:%s' % (border, level)
        logger.info(info)
        border_path = utils.getpath('../data/borders/%s.txt' %
                                    border)
        if os.path.isfile(border_path):
            border_data = open(border_path, 'r').readlines()
        else:
            return

        border_tiles = utils.get_deg2num(level, border_data)
        self.border_file_bin = utils.get_binary(border_tiles)
        binary_data = utils.get_border_binary(border, level)
        self.bbox = utils.getbbox(border_tiles)
        seed_pid_file = utils.getpath("../logs/seed.pid")
        lock = multiprocessing.Lock()
        processes_list = list()
        ############# Tile gen process #################
        tile_gen_processes = list()
        for host in xrange(NUMBER_OF_PROCESSES):
            p = multiprocessing.Process(target=self.tile_generator,
                                        args=(level, host, NUMBER_OF_PROCESSES, lock))
            p.start()
            #p.daemon = True
            processes_list.append(p.pid)
            tile_gen_processes.append(p)
            with open(seed_pid_file, 'a') as pf:
                pf.writelines(str(p.pid) + '\n')

        #################################################
        self.level = level
        ######## Start download thread #################
        # self.start_fetch_pool()
        #failed = multiprocessing.Queue()
        #fetch_process = multiprocessing.Process(target=self.start_fetch_pool)
        #fetch_process.start()
        #processes_list.append(fetch_process.pid)

        self.start_fetch_pool()
        #with open(seed_pid_file, 'a') as pf:
        #    pf.writelines(str(fetch_process.pid) + '\n')
        print >> sys.stderr, '\n\tWaiting to finish Tile Generator Agent ...',
        for process in tile_gen_processes:
            process.join()
        print >> sys.stderr, '\tDone.'
        #fetch_process.join()
        self.pool.join()


        ################################################
        #if self.failed_urls:
        #    print '\n\n\tNow trying to seed failed urls once more\n'
        #    self.remaining_pool = Pool(cc)
        #    for i in self.failed_urls:
        #        self.remaining_pool.spawn(self.fetch, i, self.args.timeout[0])

        #self.remaining_pool.join()

        #if self.failed_urls:
        #    print '\n\n\tfinished without completing all tiles for level %s. :(' % level

        # some cleanups
        cleancmd = 'kill %s' % ' '.join([str(p) for p in processes_list])
        utils.process(cleancmd)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        prog='ctcseed', description="Chista Cache Server Seeder Tool")
    parser.add_argument("-l", "--layer", type=str, action="store",
                        help="Layer name to seed", default=False)
    parser.add_argument("-b", "--border", type=str, action="store",
                        help='Coverage border name. Overwrites bbox/bboxlist options')
    parser.add_argument("-z", '--zoom',  type=int, nargs=2, action="store",
                        help="Zoom levels [start end]")
    parser.add_argument("-p", "--port", type=int, action="store",
                        help='Port number of NGINX ctc server. Default is 8085', default=8085)

    parser.add_argument("-f", '--force',  action="store_true",
                        help="Force re-render existing caches", default=False)
    parser.add_argument("-k", '--harakiri',  action="store_true",
                        help="Force kill all seeders", default=False)
    parser.add_argument(
        "-bbox", '--boundingbox',  type=float, nargs=4, action="store",
        help="Bounding Box [minx miny maxx maxy]. Overwrites bboxlist option")
    parser.add_argument("-bn", "--bboxname", type=str, action="store",
                        help='A unique name for a given boundingbox')
    parser.add_argument("-bbl", "--bboxlist", type=str, action="store",
                        help='Path to file containing bbox list. \
                        each line must be "name,minx,miny,maxx,maxy"')
    parser.add_argument("-if", '--image_format',  type=str, action="store",
                        help="Set Image format of seeding. [png jpg svg]\
                        If not specified, ctcseed would try to look at your config file\
                        to find 'force_format' variable")
    parser.add_argument("-u", '--url',  type=str, action="store",
                        help="Set base url to start. Default is 127.0.0.1", default='127.0.0.1')
    parser.add_argument("-urp", '--url_record_path',  type=str, action="store",
                        help="NOT WORKING! Path to record urls. Wont seed if specified")
    parser.add_argument("-ss", '--Save_Seed',  type=str, action="store",
                        help="NOT WORKING! Path to a folder to save seeded tiles. will create if not exsisted")
    parser.add_argument("-t", '--testmode',  action="store_true",
                        help="NOT WORKING! Test seeding process. Requests will be send to \
                        tile.openstreetmap.com/{z}/{x}/{y}.png. \
                        Overwrites port, layer, force and image_format.",
                        default=False)
    parser.add_argument(
        "-cc", '--concurrent_download_size',  type=int, nargs=1, action="store",
        help="Pool size for concurrent downloads [int], Default is 20\
        maximum is 128", default=[20])
    parser.add_argument(
        "-to", '--timeout',  type=float, nargs=1, action="store",
        help="Timeout for requests. Default is 15.0 seconds", default=[15.0])
    parser.add_argument("-d", "--debug", type=str, action="store",
                        help="Debug severity level. Default is debug", default='debug')

    parser.add_argument("-g", '--graph',  action="store_true",
                        help="Create cache graphs in background and create an animated gif", default=False)
    parser.add_argument(
        "-gf", '--graph_frequency',  type=int, nargs=1, action="store",
        help="Graph creation frequency. default is 15 seconds", default=[15])
    parser.add_argument(
        "-gd", '--graph_duration',  type=int, nargs=1, action="store",
        help="Duration of each graph slide. default is 40 milliseconds", default=[40])
    parser.add_argument("-gb", "--graph_border", type=str, action="store",
                        help='Coverage border for graphing option')
    args = parser.parse_args()

    borders = list()
    seed_pid_file = utils.getpath("../logs/seed.pid")
    if args.debug:
        if args.debug.lower() in ['debug', 'info', 'warning', 'error', 'critical']:
            logger.setLevel(eval('logging.%s' % args.debug.upper()))
    if args.harakiri:
        kill_me(message=True)
    if args.harakiri and not args.layer:
        sys.exit()

    if args.graph and not args.graph_border:
        print '\tYou need to specify a graph border along with graph option with -gb'
        sys.exit()
    if not args.layer:
        print '\tYou need to specify a layer name! Use -l'
        sys.exit()
    if args.url_record_path:
        print '\tNotice: Using -udp will trigger dry mode implicit.'
    else:
        layer_config = Config.get(args.layer)
        if not layer_config:
            print '\tWrong Layer!'
            simils = get_similar_layer(args.layer)
            if simils:
                print '\tI found similar layers: [%s] | Please fix the command' % ','.join(simils)
            sys.exit()

    if not args.zoom:
        print '\tYou missed zoom levels to seed! Use -z'
    if not (args.border or args.bboxlist or (args.boundingbox and args.bboxname)):
        print '\tAny borders? Use -[b/bbox/bbl]'
    if not args.image_format:
        print '\tNo image format! Let me look for it in config file ...'
        if layer_config.get('force_format'):
            args.image_format = layer_config.get('force_format')[0]
            print '\tOk, good. I found a default format: %s' % args.image_format
            debug = '{section}|Using config image force_format|{ext}'\
                .format(section=args.layer, ext=args.image_format)
            logger.debug(debug)
        else:
            error = '{section}|No Image format specified'.format(
                section=args.layer)
            logger.error(error)
            print '\t', error
            sys.exit()

    if not (args.zoom and (args.layer or args.testmode) and (args.border or
            args.bboxlist or (args.boundingbox and args.bboxname))):
        print '\n'
        parser.print_usage()
        print '\n'
    else:
        batch_mode = False
        batch_counter = 0
        if args.boundingbox:
            minx, miny, maxx, maxy = args.boundingbox
            args.border = '.' + args.bboxname
            utils.bboxtopoly(args.border, minx, miny, maxx, maxy)
            borders.append(args.border)

        elif args.bboxlist:
            bbox_lines = open(args.bboxlist, 'r').readlines()
            for line in bbox_lines:
                datalist = map(str.strip, line.split(','))
                # print datalist
                if line.strip():
                    name, minx, miny, maxx, maxy = datalist[:5]
                    args.border = '.' + name
                    utils.bboxtopoly(args.border, minx, miny, maxx, maxy)
                    borders.append(args.border)
            print '\tSeeding %s bboxes:' % len(borders)
            for B in borders:
                print '\t\t%s' % B
        else:
            borders = [args.border]

        r = list(set(args.zoom))
        r = sorted(r)
        pid = os.getpid()
        with open(seed_pid_file, 'a') as pf:
            pf.writelines(str(pid) + '\n')
        print '\tYou can {t.red}kill seeder{t.normal} by running {t.bold}{t.green}CTRL+c{t.normal} or {t.green}ctcseed -k{t.normal}'\
            .format(t=term)
        stime = time.time()
        seed = Seed(args, args.testmode)
        args_info = repr(args.__dict__)[1:-1].replace(',', '|')
        logger.info('starting seed with: %s' % args_info)
        if not args.testmode:
            cachedir = seed.config.get('cache')
            if isinstance(cachedir, list):
                cachedir = cachedir[0]
            cachedir = cachedir + '/' + args.layer
        else:
            cachedir = '/home'
        for z in xrange(r[0], r[-1] + 1):
            if args.graph:
                v = multiprocessing.Process(target=make_gif, args=(args, z, args.graph_border))
                v.start()
                with open(seed_pid_file, 'a') as pf:
                    pf.writelines(str(v.pid) + '\n')
            for border in borders:
                try:
                    border_data = open(utils.getpath('../data/borders/%s.txt') %
                                       border, 'r').readlines()
                except IOError:
                    print '\tLayer not found (%s)! Copy border data to data/borders folder.' % border
                    sys.exit()
                if args.Save_Seed:
                    cachedir = args.Save_Seed
                
                ignore = False
                try:
                    if args.bboxlist:
                        data = open(args.bboxlist, 'r').readlines()
                        for ln in xrange(len(data)):
                            # print data[ln].split(',')[0]
                            if data[ln].split(',')[0] == border[1:]:

                                done_list = map(
                                    int, map(str.strip, data[ln].split(',')[5:]))
                                if z in done_list:
                                    print '\n\t level %s is marked as cached. Ignoring ...' % z
                                    ignore = True
                                    # data.remove(data[ln])
                                else:
                                    data[ln] = data[ln].strip() + ',%s\n' % z
                    if not ignore:
                        try:

                            seed.start_seed(border, z)

                        except KeyboardInterrupt:
                            print '\tCancelling Seed ... Goodbye!'
                            kill_me()
                            sys.exit()

                    if args.bboxlist:
                        try:
                            open(args.bboxlist, 'w').writelines(data)
                            #os.remove(utils.getpath('../data/borders/%s.txt' % border))
                        except IOError, e:
                            print e
                            pass

                    #seedme(z, border_data, cachedir, args)
                except KeyboardInterrupt:
                    print '\tCancelling Seed ...\n\tGoodbye!'
                    kill_me()
                    sys.exit()
            if args.graph:
                print >> sys.stderr, '\n\tTerminating graphing process ...',
                utils.process('kill %s' % v.pid)
                print >> sys.stderr, '\tDone.',
        totaltime = round(time.time() - stime, 3)
        print "\n\tSeeding finshed in {t.green}{t.bold}{time}{t.normal} seconds!\n\t{t.green}Goodbye.{t.normal}\n"\
            .format(t=term, time=totaltime)
        logger.info('Seed Finished|%s' % totaltime)


'''
ctcseed -bbox 35.5 51.05 35.85 51.7 -bn ok -l chista -z 15 15
ctcseed -b tehran -l chista -z 15 15
'''
