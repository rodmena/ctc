#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology! 
Clean code is much better than Cleaner comment
'''


import os
import sys
#import cPickle as pickle
import time
#from bisect import bisect_left, bisect_right
import collections
import mmap
import re
import struct
import cProfile as profile
import timeit


def mapfile(filename):
    '''
    @param filename: a path to a file
    @return: mmap
    @usage:
        data = mapfile("samples/sample.txt")
    '''
    f = open(filename, "rb+")
    size = os.path.getsize(filename)
    return mmap.mmap(f.fileno(), size)


class Searcher:

    '''
    A very nice binary searcher
    '''

    def __init__(self, filename):
        # self.f = open(filename, 'r'i)
        self.s = time.time()
        self.f = mapfile(filename)
        self.f.seek(0)
        self.length = self.f.tell()
        self.result = collections.defaultdict(list)
        self.size = self.f.size()
        self.chunk = 8

    def find(self, searchFor, startIndex=0, endIndex=None):
        chunk = self.chunk
        if endIndex is None:
            endIndex = self.size  # 2x4bytes per line
        while startIndex < endIndex:
            mid = (startIndex + endIndex) // 2
            mid -= mid % chunk
            self.f.seek(mid)
            midval = struct.unpack('2i', self.f.read(chunk))
            #print >> sys.stderr, midval
            if midval[0] < searchFor:
                startIndex = mid + (chunk)
            elif midval[0] > searchFor:
                endIndex = mid
            else:  # Ok, I found the first one. lets find others.
                return self.find_x_group(searchFor, mid)
#            if self.f.tell() + chunk >= self.size:
#                break

        return self.result  # This shouldnt be called.

    def find_x_group(self, searchFor, mid):
        # print 'Now finding'
        res = set()
        chunk = self.chunk
        for d in [0, -2]:
            self.f.seek(mid)  # go to corect correction
            while True:
                val = struct.unpack('2i', self.f.read(chunk))
                # print val
                go = self.f.tell() + (chunk * d)
                # print go,self.size
                if val[0] == searchFor:
                    res.add(val[1])
                if go < self.size and go >= 0:
                    self.f.seek(go)
                else:
                    break

        self.result[searchFor] = sorted(list(res))

        #print >> sys.stderr, 'Binary search time: ', time.time()-self.s, 'Binary search Result for x=:', searchFor,  self.result
        return self.result


def get_file_path(folder, zoom):
    '''give a zoom level and folder and get a filename'''
    zoom_levels = os.listdir(folder)
    pattern = re.compile(r'[a-zA-Z]+-[0]*%s\.[a-z]{3}\.bin' % zoom)
    matches = re.findall(pattern, '.'.join(zoom_levels))
    if not matches:
        return False
    filepath = folder + os.path.sep + matches[0]
    return filepath


def filter_mylist(ylist):
    '''Combine items that are +|- 1'''
    # ylist = sorted(list(set(ylist)), reverse=False)  ## trick to remove
    # duplicates
    if not ylist:
        return []
    result = list()
    for i in range(len(ylist) - 1):
        if not ylist[i] + 1 == ylist[i + 1]:
            result.append(ylist[i])
    result.append(ylist[-1])
    return result


def is_inside(xy, raw):
    '''Get a sorted tupple based on saved border data and new xy
    @return: list
    @param raw: The raw data from broder limit files
    @param z: Zoom level
    @param xy: the tile in tupple format. ex: (10,50)
    '''

    #start = time.time()
    # TODO better way?
    #print >> sys.stderr, raw
    y = xy[1]
    if not xy[0] in raw.keys():
        return -1  # NOT there? NOTE
    data = raw[xy[0]]
    # print data
    if y in data:  # it's there  no need to do more
        return 0

    data.append(y)  # add y
    data.sort()  # resort
    pos = data.index(y)
    left_list = data[:pos]
    right_list = data[pos + 1:]
    # if not (left_list and right_list) and (left_list or right_list):
    # return 1  # Probabaly it's there  NOTE please

    left_list = filter_mylist(left_list)  # filter corners
    right_list = filter_mylist(right_list)  # filter corners
    #print >> sys.stderr, left_list, right_list
    if (len(left_list) % 2 or len(right_list) % 2):
        return 1
    else:
        return -1


def where(tile, zoom, folder):
    #raw = get_border_data('/usr/local/lib/python2.7/dist-packages/TileCache-2.11-py2.7.egg/TileCache/data/Borders/')
    #zoom = 14
    filepath = get_file_path(folder, zoom)
    s = Searcher(filepath)
    raw = s.find(tile[0])
    # print 'finding',  raw
    # print raw
    return is_inside(tile, raw)

if __name__ == '__main__':
    '''tile: (10531, 6452) zoom: 14'''
    #raw = get_border_data('/usr/local/lib/python2.7/dist-packages/TileCache-2.11-py2.7.egg/TileCache/data/Borders/')
    #tile = (10531, 6452)
    #zoom = 14
    #get_sorted_xy(tile, zoom, raw)
    # tile: (5266, 3226) zoom: 13
    #ylist = [3194, 3226, 3448]
    #small_list = filter_mylist(ylist)
    # print is_it_inside(3226, small_list)
    folder = 'data/Borders'
    where((10531, 6452), 14, folder)
    #timeit.timeit("where((10531, 6452), 14, folder)", number=10000)
