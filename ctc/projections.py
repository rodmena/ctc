#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-


_copyright = 'Farsheed Ashouri'
'''
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an appology! 
Clean code is much better than Cleaner comments!
'''
import os
import sys
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
import math
import utm
import ctc.packages.tilenames as tilenames


def _c4326t3857(lon, lat):
    """
    Pure python 4326 -> 3857 transform. About 8x faster than pyproj.
    """
    lat_rad = math.radians(lat)
    xtile = lon * 111319.49079327358
    ytile = math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / \
        math.pi * 20037508.342789244
    return(xtile, ytile)


def _c3857t4326(lon, lat):
    """
    My Pure python 3857 -> 4326 transform. About 12x faster than pyproj. :)
    """
    xtile = lon / 111319.49079327358
    ytile = math.degrees(
        math.asin(math.tanh(lat / 20037508.342789244 * math.pi)))
    return(xtile, ytile)


def bounds(z, x, y):
    '''Get bbox based on tile values'''
    NW = tilenames.num2deg(x, y, z)
    SE = tilenames.num2deg(x + 1, y + 1, z)
    maxx = max(NW[0], SE[0])
    maxy = max(NW[1], SE[1])
    minx = min(NW[0], SE[0])
    miny = min(NW[1], SE[1])
    miny, minx = _c4326t3857(miny, minx)
    maxy, maxx = _c4326t3857(maxy, maxx)
    return [miny, minx, maxy, maxx]


def bbox(z, x, y):
    bbox = bounds(z, x, y)
    return ','.join(map(str, bbox))


def to_utm(lat, lon):
    '''Get utm conversion'''
    return utm.from_latlon(lat, lon)
