#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology! 
Clean code is much better than Cleaner comments!
'''


'''
@desc: seed.py
@c: Chista Co
@author: F.Ashouri
@version: 0.0.5
'''

import os
import sys
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
import ctc.utils as utils
import time
from PIL import Image  # for reading images.
from PIL import ImageDraw
from collections import defaultdict
from glob import glob
from ctc.config import Config
import cPickle as pickle
import argparse
import sys
import tempfile
from greenlet import greenlet


class Show(object):

    '''Graphing and visualazation class'''

    def __init__(self, section, border, ext=None):
        self.border_data = open(utils.getpath('../data/borders/%s.txt') %
                                border, 'r').readlines()
        self.section = section
        self.border = border
        rates = {'iran': 0.57495313207}
        self.rate = 1
        if border in rates.keys():
            self.rate = rates.get(border)  # aproximate
            # print self.rate
        self.config = Config().read().get(section)
        if not self.config:
            print '\tWrong layer name!'
            sys.exit()
        self.ext = ext
        if self.ext == 'all':
            self.ext = '*'
        if not ext and self.config.get('force_format'):
            self.ext = self.config.get('force_format')[0]
        if not self.ext:
            print '\tPlease specify an image format.'
            sys.exit()

        mc = self.config.get('cache')
        if isinstance(mc, list):
            mc = mc[0]
        if not (self.config.get('cache_exact_path') and self.config.get('cache_exact_path')[0] == 'true'):
            self.cachedir = os.path.join(mc, section)
        else:
            self.cachedir = mc

    def __del__(self):
        '''cleanup'''
        # self.db.commit()
        # self.db._adapter.close_all_instances(None)

    def vis(self, zoom, user_defined_text=None, verbose=True):
        st = time.time()
        border_tiles = utils.get_deg2num(zoom, self.border_data)
        # print border_tiles
        minx, miny, maxx, maxy = utils.getbbox(border_tiles)
        WIDTH = 800
        x = (maxx - minx) + 1
        y = (maxy - miny) + 1
        total = x * y

        X, Y = x, y
        if x > WIDTH:
            X, Y = WIDTH, (WIDTH * y) / x
        xratio = float(X) / float(x)
        yratio = float(Y) / float(y)
        ########### New method #####################
        points = list()
        s = time.time()
        for i in xrange(minx, maxx + 1):
            # print i
            b = miny
            counter = 0
            gat = greenlet(utils.get_available_tiles)
            avail = gat.switch(self.cachedir, zoom, i, ext=self.ext)
            msgper = round(((i - minx) * 100.0) / (maxx - minx), 0)
            msg = '\tprocess: ' + str(msgper) + '% ' + '-' * int(msgper / 2)
            if verbose and not msgper%3:
                sys.stdout.write(msg)
                sys.stdout.write('\r' * len(msg))
                sys.stdout.flush()
            points.extend(avail)
        sys.stdout.write('\r' * len(msg) * 2)
        if verbose:
            print '\n\tDone Searching Hard Drive'
        ########### Search for files ################
        #points = utils.get_available_tiles(self.cachedir, zoom)
        # print len(points)
        ########### check if inside ##################
        #available = utils.get_tiles_inside_border(points, self.border, zoom)
        ##############################################
        available = (i for i in points if (
            i[0] <= maxx and i[0] >= minx and i[1] >= miny and i[1] <= maxy))
        #available = points
        available = (( int((i[0]-minx)*xratio), int((i[1]-miny)*yratio) ) for i in available)

        te = Image.new("RGB", (X, Y - 1 or Y), (31, 31, 31, 0))  # grey
        draw = ImageDraw.Draw(te)
        #g = defaultdict(list)
        poly = ((int((xy[0] - minx) * xratio), int((xy[1] - miny) * yratio))
                for xy in border_tiles)
        #pminx, pminy, pmaxx, pmaxy = utils.getbbox(poly)
        # print poly
        draw.polygon(list(poly), fill=(41, 41, 41))  # light grey

        for xy in available:
            fill = (68, 133, 244)  # blue
            draw.point(
                (xy[0], xy[1]), fill=fill)

        te = te.resize((WIDTH, (WIDTH * y) / x)) ## for small scale fixes
        imsize = te.size
        availen = len(list(available))
        per = (availen * 100.0) / float((x * y) * self.rate)
        if per > 1:
            per = round(per, 2)
        if not per % 10:
            per = int(per)
        # if per > 100:
        #    per = 100
        #    total = len(available)
        else:
            per = round(per, 6)
        te = utils.addtext(
            te, 'border:{border} on zoom: {zoom}'.format(
                border=self.border.title(), zoom=zoom),
            x=20, y=5, size=16, color=(190, 190, 190))
        if per:
            te = utils.addtext(
                te, '{per}% Cached'.format(per=per), x=20, y=20, size=44, color='white')
            te = utils.addtext(te, '||{eq}{dot}||'.format(
                eq='#' * int(per), dot='...' * int(100 - per)), x=20, y=26, size=5, color='white')
        else:

            te = utils.addtext(
                te, 'EMPTY :(', x=20, y=20, size=36, color='white')
        te = utils.addtext(
            te, '{tiles} tiles out of {total} tiles have been chached so far.'.format(tiles=availen, total=int(total*self.rate)), x=20, y=72, color=(190, 190, 190))
        if user_defined_text:
            te = utils.addtext(te, user_defined_text, x=20,
                               y=100, size=24, color=(190, 190, 190))
        te = utils.addtext(
            te, '{time}'.format(time=time.ctime()), x=20, y=imsize[1] - 48, color=(190, 190, 190))
        te = utils.addtext(
            te, 'Image created in {time} secs by Chista CTC version {v}'.format(time=round(time.time() - st, 4),
                                                                                v=open(utils.getpath('../VERSION'), 'r').read().strip()), size=12, x=20, y=imsize[1] - 25, color=(181, 181, 181))

        return te
        # return path


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='ctcshow', description="Chista Cache Server graph utility")
    parser.add_argument("-l", "--layer", type=str, nargs=1, action="store",
                        help="Layer name to clean.")
    parser.add_argument("-z", '--zoom',  type=int, nargs=1, action="store",
                        help="Zoom level [int]")
    parser.add_argument("-b", '--border',  type=str, nargs=1, action="store",
                        help="border [string]")
    parser.add_argument("-p", '--path',  type=str, nargs=1, action="store",
                        help="Path to save png image [string]")
    parser.add_argument("-t", '--text',  type=str, nargs=1, action="store",
                        help="Custom text to add [string]", default=[''])
    parser.add_argument("-if", '--image_format',  type=str, action="store",
                        help="Set Image format of seraching. [png jpg svg]\
                        If not specified, ctcseed would try to look at your config file\
                        to find 'force_format' variable [jpg png svg all]")
    parser.add_argument("-v", '--verbose',  action="store_true",
                        help="verbose mode, print more info than normal", default=True)

    args = parser.parse_args()
    if not (args.layer and args.zoom and args.border):
        parser.print_usage()
        sys.exit()

    s = Show(args.layer[0], args.border[0], args.image_format)
    vis = s.vis(args.zoom[0], args.text[0], args.verbose)
    if args.path:
        vis.save(args.path[0], format='PNG')
        print('\tDone! Saved to %s' % args.path[0])
    else:
        vis.show()
