#!/usr/bin/env python
# -*- coding: utf-8 -*-

_copyright = 'Chista Co'
"""
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology! 
Clean code is much better than Cleaner comments!

"""

'''
what we need? Ok, We need a tool to 

'''


import os
import sys
import utils
import argparse
from PIL import Image
import shutil

'''
@usage:
    For memory leak prevention, use this command in terminal before running ctcsimilars:
>>> ulimit -Sv 500000     # Set ~500 mb limit
'''


def detect(folderpath, delete=False):
    '''Walk in folder path'''
    corrupted = 0
    similars = 0
    print '\tlooking into "%s" folder ...' % folderpath
    for path, folder, files in os.walk(folderpath, followlinks=True):
        ## ...
        for file in files:
            ext = file.split('.')[-1]
            filepath = os.path.join(path, file)
            if 'SOLID' in filepath:
                continue
            try:
                img = Image.open(filepath)
            except:
                corrupted += 1
                if delete:
                    os.remove(filepath)
                continue
            colors = utils.get_color_list(img, raw=True, ext=ext)
            del img
            if len(colors) == 1:
                similars += 1
                solidname = 'SOLID/%s.%s' % (colors[0],
                        ext.replace('png8', 'png'))
                solidpath = os.path.join(folderpath, solidname)
                relsolidpath = os.path.relpath(solidpath, start=path)
                if not os.path.isfile(solidpath):
                    try:
                        shutil.copyfile(filepath, solidpath)
                    except IOError:
                        pass

                os.remove(filepath)
                try:
                    os.symlink(relsolidpath, filepath)
                    print >> sys.stderr, '.',
                except OSError:
                    print >> sys.stderr, 'E',
                    pass
    print '\t%s similars detected.' % similars

                






if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='ctcsimilars', description="Chista Cache Server Similarity detection tool")

    parser.add_argument("-p", "--path", type=str, action="store",
                        help="Cached layer folder path", default=False)
    parser.add_argument("-d", "--delete", action="store_true",
                        help="Delete corrupted files", default=False)

    args = parser.parse_args()
    if not args.path:
        parser.print_usage()
        print 'Use this command:'
        print '>>> ulimit -Sv 500000'
        print 'For any possible memory leak prevention'
    else:
        detect(args.path, args.delete)
