#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an appology!
Clean code is much better than Cleaner comments!
'''

import projections


class Tile(object):
    '''Tile class'''
    def __init__(self, x, y, z):
        ''''''
        self.x = x
        self.y = y
        self.z = z
        self.heigth = 256
        self.width = 256
        self.ext = 'png' ## format
        self.fmt = 'image/png8'
        self.url = None
        self.bbox = projections.bbox(self.x, self.y, self.z)



