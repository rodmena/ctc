#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology!
Clean code is much better than Cleaner comments!
'''

'''
@desc: seed.py
@c: Chista Co
@author: F.Ashouri
@version: 0.1.8
'''



import requests
import logging
import utils
import sys
import os

masterpath = os.path.dirname(os.path.dirname(__file__))

#BROKER_URL = 'amqp://guest@localhost'
BROKER_URL = 'redis://localhost:6379/0'
BACKEND_URL = 'redis://localhost:6379/1'
from celery import Celery

app = Celery('Seed',
                broker=BROKER_URL,
                backend='amqp',
                include=[])

logger = utils.setup_logger('tasks', 'logs/tasks.log')


@app.task
def fetch(url, timeout=300, mode='get'):
    try:
        func = requests.get
        if mode == 'head':
            func = requests.head
        response = func(url, timeout=timeout, stream=False)
        elapsed = response.elapsed.microseconds / 1000000.0
        server_name = response.headers.get('server')
        reason = response.reason
        st = response.status_code
        ctcerror = response.headers.get('ctc-error') or 'OverLoaded NGINX/CTC!'

        if 'ctc' in server_name.lower():
            server_name = 'CTC SERVER'
        else:
            server_name = 'STATIC SERVER'
        if response.status_code in [200, 302]:
            info = '200|{reason}|Cache Saved|{url}|{el}|{server}'\
                .format(reason=reason, el=elapsed, 
                        url=url, server=server_name)
            logger.debug(info)
            return '200 OK'
        else:
            error = '{st}|{reason}|{ctcerror}|NOT Saved|{url} |{el}'\
                .format(st=st, reason=reason,
                        el=elapsed, url=url, ctcerror=ctcerror)
            logger.error(error)
            return '%s %s' % (st, reason)


    except requests.Timeout:
        warning = '504|Gateway Timeout|NOT Saved|{url}'\
                .format(url=url)
        logger.warning(warning)
        return '504 Gateway Timeout'
        #failed.put(url)

    except requests.RequestException:
        critical = '500|Internal Server Error|An Error in connecting or fetching!|NOT Saved|{url}'\
                .format(url=url)
        logger.critical(critical)
        return '500 Internal Server Error' 

    except Exception, e:
        error = e
        logger.error(error)
        #failed.put(url)
        return '500 Internal Server Errors (%s)' % e


