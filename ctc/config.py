#!../../ctcdev/bin/python -OO
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology!
Clean code is much better than Cleaner comments!
'''

import ConfigParser
import os
import sys
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
import urllib2
from random import shuffle
no_uwsgi = False
try:
    import uwsgi
except ImportError:
    no_uwsgi = True
from blessings import Terminal
term = Terminal()
import logging
import ctc.utils as utils


cfolder = os.path.join(os.path.dirname(__file__), '../config')
ctcroot = os.path.abspath(os.path.dirname((os.path.join(__file__, '../../'))))
cname = 'ctc.ini'

logger = utils.setup_logger('ctc', '%s/logs/ctc.log' % ctcroot)
apilog = utils.setup_logger('api', '%s/logs/api.log' % ctcroot)


class Config(object):

    '''CTC configuration reader class'''

    def __init__(self):
        '''constructor'''
        self.config = ConfigParser.RawConfigParser()
        #self.config = ConfigParser.RawConfigParser(allow_no_value=True)
        self.cpath = cfolder + os.path.sep + cname

        #logging.basicConfig(filename="logs/ctc.log", level=logging.ERROR),
        #            format='%(asctime)s [%(module)s] %(message)s')

        # print self.cpath

    def __del__(self):
        '''Cleanups'''

    def changetime(self):
        return os.stat(self.cpath).st_mtime

    def get_cache_folder(self, layer):
        '''Find cache_folder'''
        conf = self.read().get(layer)
        if conf:
            cache_path = conf.get('cache')
            if isinstance(cache_path, list):
                cache_path = cache_path[0]

            postfix = os.path.sep + layer
            if conf.get('cache_exact_path'):
                postfix = ''

            return cache_path + postfix

    def read(self):
        '''Read everything in the layer and return it as a dict'''
        slots = ['global_settings']
        # print '\n\t{t.yellow}Loading Config:{t.normal}
        # {t.cyan}{t.bold}{cpath}{t.normal}'.format(cpath=os.path.abspath(self.cpath),
        # t=term)
        self.config.read(self.cpath)
        data = dict()
        ############# debug level ##############
        self.debug_level = 'INFO'
        defaults = ['cache', 'timeout', 'cache_age', 'client-max-age',
                    'logo', 'logo_frequency', 'proxy', 'quality']
        if self.config.has_section('global_settings'):
            for default_option in defaults:
                if self.config.has_option('global_settings', default_option):
                    data[default_option] = self.config.get(
                        'global_settings', default_option)
            if self.config.has_option('global_settings', 'debug_level'):
                dl = self.config.get('global_settings', 'debug_level')
                if dl:
                    self.debug_level = dl.strip().upper()
                # print self.debug_level
                logger.setLevel(eval('logging.%s' % self.debug_level))
                apilog.setLevel(eval('logging.%s' % self.debug_level))
        #########################################
        data['saved'] = self.changetime()

        data['cfg'] = self.cpath
        for each in self.config.sections():
            # print '\tLoading layer %s ...' % each
            if not each in slots:
                data[each] = {}
                for i in self.config.items(each):
                    if i[0] == 'proxy' \
                            and self.config.get(each, 'proxy') and not no_uwsgi:
                        in_memory_proxies = uwsgi.cache_get(
                            '%s_proxies' % each)
                        url = i[1].strip()
                        if url and not in_memory_proxies:
                            url = url.strip()
                            mode = 'local'
                            for sch in ['http', 'https', 'ftp']:
                                if sch in url:
                                    try:
                                        r = urllib2.urlopen(url)
                                        if r.code == 200:
                                            pl = map(str.strip,
                                                     r.readlines())
                                            mode = 'online'
                                            break
                                    except Exception, e:
                                        print '\tCant download proxy list. %s' % e
                                        pl = list()
                                        mode = 'online'
                                        break
                            if mode == 'local':
                                datafolder = os.path.abspath(
                                    os.path.join(os.path.dirname(__file__), '../data'))
                                pl = map(
                                    str.strip, open(url.replace('{data}', datafolder), 'r').readlines())
                            pl = [x for x in pl if x]

                            for pr in pl:
                                utils.addto_urls_database(
                                    '%s_proxies' % each, pr)
                        elif not self.config.getboolean(each, 'use_proxy'):
                            # empty proxy
                            uwsgi.cache_update('%s_proxies' % each, "")
                    data[each][i[0].lower()] = map(str.strip, i[1].split(','))

                for default_option in defaults:
                    if not data[each].get(default_option):
                        data[each][default_option] = data.get(default_option)

        return data

    def get_cpath(self):
        return self.cpath

if __name__ == '__main__':
    '''Some tests'''
    config = Config()
    print config.read()
