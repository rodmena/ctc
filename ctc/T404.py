#!/usr/bin/env python
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _ 
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology! 
Clean code is much better than Cleaner comments!
'''

import os

status = ('200 OK')
ct = ('Content-type', 'image/jpeg')
srv = ('Server', 'Chista Cache Server')
data = open('data/images/404.jpg', 'rb')


def app(environ, start_response):
    block_size = os.path.getsize(data.name)
    response_headers = [ct, ('Content-Length', str(block_size)), srv]
    start_response(status, response_headers)
    if 'wsgi.file_wrapper' in environ:
        return environ['wsgi.file_wrapper'](data, block_size)
    else:
        return iter(lambda: data.read(block_size), '')
