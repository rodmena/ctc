#!../../ctcdev/bin/python
# -*- coding: utf-8 -*-

_copyright = 'Chista Co.'
'''
   ___              _                   _
  / __\_ _ _ __ ___| |__   ___  ___  __| |
 / _\/ _` | '__/ __| '_ \ / _ \/ _ \/ _` |
/ / | (_| | |  \__ \ | | |  __/  __/ (_| |
\/   \__,_|_|  |___/_| |_|\___|\___|\__,_|

Just remember: Each comment is like an apology!
Clean code is much better than Cleaner comment
'''


'''
@desc: seed.py
@c: Chista Co
@author: F.Ashouri
@version: 0.0.1
'''

import os
import sys
#sys.path.append()
fp = os.path.dirname(__file__)
sys.path.append(os.path.dirname(fp))
import ctc.config as config
import argparse
from blessings import Terminal
import requests
import gzip
from cStringIO import StringIO
term = Terminal()
import ctc.utils as utils

Config = config.Config().read()


class Clean(object):

    '''Cleanup tools'''

    def __init__(self, layer, image_format=None, dry=False):
        self.layer = layer
        self.image_format = image_format
        self.dry = dry
        self.config = Config.get(layer)
        if not self.config:
            print '\tWrong layer name!'
            sys.exit()
        self.cache_folder = self.config.get('cache')
        if isinstance(self.cache_folder, list):
            self.cache_folder = self.cache_folder[0]
        if not (self.config.get('cache_exact_path') and self.config.get('cache_exact_path')[0] == 'true'):
            self.cachedir = os.path.join(self.cache_folder, layer)
        else:
            self.cachedir = self.cache_folder
        if self.config.get('force_format') and not image_format:
            self.image_format = self.config.get('force_format')[0]
        if self.image_format == 'all':
            self.image_format = '*'
        print '\n\tChecking %s layer ...' % layer

    def __del__(self):
        '''Cleanup'''
        print '\tGoodbye!\n'

    def remove_level(self, z):

        remove_folder = os.path.abspath(
            os.path.join(self.cachedir, str(z)))
        if os.path.isdir(remove_folder) and not self.dry:
            os.system('rm -rf "%s"' % remove_folder)
            print '\tSuccessfully deleted zoom level %s' % z
        elif not os.path.isdir(remove_folder):
            print "\tSeems you deleted level %s before! It's empty" % z

    def remove_layer(self):
        '''remove anything related to later'''
        print '\tDeleting "%s" layer from disk ...' % self.layer
        remove_folder = os.path.abspath(self.cachedir)
        if os.path.isdir(remove_folder) and not self.dry:
            os.system('rm -rf "%s"' % remove_folder)
        elif not os.path.isdir(remove_folder):
            print '\tSeems I deleted it before!'

        print '\tDone.'

    def remove_tiles(self, tiles, z):
        '''Tiles must be list of tuples [(x1,y1), (x2,y2), ...]'''
        if not self.image_format:
            print '\tPlease set an image format with -if option'
            sys.exit()
        if not tiles:
            return
        count = 0
        for tile in tiles:
            remove_file = os.path.abspath(
                os.path.join(self.cachedir, str(z),
                             str(tile[0]), str(tile[1]) + '.' + self.image_format))
            if os.path.isfile(remove_file) and not self.dry:
                os.remove(remove_file)
                count += 1
        print '\t%s tiles removed.' % count

    def remove_bbox(self, minx, miny, maxx, maxy, zoom):
        if not self.image_format:
            print '\tPlease set an image format with -if option'
            sys.exit()
        bp = utils.bboxtopoly('.clean', minx, miny, maxx, maxy)
        border_data = open(
            utils.getpath('../data/borders/.clean.txt'), 'r').readlines()
        border_data = [i.strip() for i in border_data if i.strip()]
        border_tiles = utils.get_deg2num(zoom, border_data)
        mintx, minty, maxtx, maxty = utils.getbbox(border_tiles)
        avails = utils.get_available_tiles(
            self.cachedir, zoom, ext=self.image_format)
        available = [i for i in avails if (
            i[0] <= maxtx and i[0] >= mintx and i[1] >= minty and i[1] <= maxty)]
        count = 0
        if available:
            print '\tI found %s tiles in [%s %s %s %s] for %s on zoom %s' % (len(available),
                                                                             minx, miny, maxx, maxy, self.layer, zoom)
        for tile in available:
            remove_file = os.path.abspath(
                os.path.join(self.cachedir, str(zoom),
                             str(tile[0]), str(tile[1]) + '.' + self.image_format))
            # print remove_file

            if os.path.isfile(remove_file) and not self.dry:
                os.remove(remove_file)
                count += 1

        print '\t%s tiles removed.' % count

    def download(self, url):
        '''Download data from internet or local address'''
        gzmode = False
        if url.split('.')[-1].lower() == 'gz':  # file is gzip:
            gzmode = True
        if os.path.lexists(url):  # case of local file
            if gzmode:
                gzin = gzip.open(url, 'rb')
                data = gzin.readlines()
                gzin.close()  # memory
                return data
            else:
                return open(url, 'r').readlines()
        r = requests.get(url, timeout=60)
        if r.status_code == 200:  # OK
            data = r.content
            if gzmode:
                buf = StringIO()
                gzin = gzip.open(buf, 'rb')
                data = gzin.readlines()
                gzin.close()  # memory

            return data

    def generate_tiles(self, url, z):
        data = self.download(url)
        if data:
            print '\tCalculating tiles for zoom %s. please wait ...' % z
            poly = utils.get_deg2num(z, data)
            # No need these two lines anymore  18feb2014
            #tiles = utils.render(poly, z)
            # return tiles
            return poly


if __name__ == '__main__':
    '''Test suite'''
    parser = argparse.ArgumentParser(
        prog='ctcclean', description="Chista Cache Server cleanup utility")
    parser.add_argument("-l", "--layer", type=str, action="store",
                        help="Layer name to clean.")
    parser.add_argument("-dry", action="store_true",
                        help="Dry run. ctcclean won't touch any files", default=False)
    parser.add_argument("-t", "--tilepath", type=str, action="store",
                        help='FTP/local path of tiles list to clean')
    parser.add_argument("-a", "--all", action="store_true",
                        help='Clean all tiles in layer. overwites -t option', default=False)
    parser.add_argument("-if", '--image_format',  type=str, action="store",
                        help="Set Image format for deleting. If not set, It will look\
                        to your config settings for 'force_format' variable\
                        [jpg png svg all]")
    parser.add_argument(
        "-bbox", '--boundingbox',  type=float, nargs=4, action="store",
        help="Delete based on a Bounding Box [minx miny maxx maxy]\
                        Overwrites -a and -t options")
    parser.add_argument("-bbl", "--bboxlist", type=str, action="store",
                        help='Path to file containing bbox list. \
                        each line must be "name,minx,miny,maxx,maxy\
                        Overwrites -bbox, -t and -a options"')

    parser.add_argument("-z", '--zoom',  type=int, nargs=2, action="store",
                        help="Zoom levels [start end]", default=False)
    parser.add_argument("-Z", '--AZ',  action="store_true",
                        help="Include All zoom levels. Overwrites -z option", default=False)
    args = parser.parse_args()
    if not args.layer:
        print '\tYou need to select a layer'

        parser.print_usage()
        sys.exit()

    if not (args.zoom or args.AZ) and args.layer and (args.tilepath or args.all
                                                      or args.boundingbox or args.bboxlist):

        parser.print_usage()
        sys.exit()
    if args.layer and not (args.tilepath or args.zoom or args.AZ or args.all):
        print '\tWhat do you want me to do with %s layer? You can use -Z with -a to delete everything!' % args.layer
        print '\tUse ctcclean -h for help.'
        sys.exit()
    if args.layer and (args.AZ or args.zoom) and not (args.tilepath or
                                                      args.all or args.boundingbox or args.bboxlist):
        print '\tWhich tiles you need to clean?'
        print '\tYou can use -a for all tiles'
        sys.exit()

    else:
        clean = Clean(args.layer, args.image_format, args.dry)

        if args.all and args.AZ:
            clean.remove_layer()
        elif args.all and args.zoom:
            r = list(set(args.zoom))
            r = sorted(r)
            for z in xrange(r[0], r[-1] + 1):
                clean.remove_level(z)
        elif args.bboxlist and args.AZ:
            for z in range(0, 21):
                bbox_lines = open(args.bboxlist, 'r').readlines()
                bbox_lines = [i.strip() for i in bbox_lines if i.strip()]
                for line in bbox_lines:
                    datalist = map(str.strip, line.split(','))
                    name, miny, minx, maxy, maxx = datalist[:5]
                    clean.remove_bbox(minx, miny, maxx, maxy, z)
        elif args.bboxlist and args.zoom:
            r = list(set(args.zoom))
            r = sorted(r)
            for z in xrange(r[0], r[-1] + 1):
                bbox_lines = open(args.bboxlist, 'r').readlines()
                for line in bbox_lines:
                    datalist = map(str.strip, line.split(','))
                    name, miny, minx, maxy, maxx = datalist
                    clean.remove_bbox(minx, miny, maxx, maxy, z)
        elif args.boundingbox and args.AZ:
            for z in range(0, 21):
                minx, miny, maxx, maxy = args.boundingbox
                clean.remove_bbox(minx, miny, maxx, maxy, z)
        elif args.boundingbox and args.zoom:
            r = list(set(args.zoom))
            r = sorted(r)
            for z in xrange(r[0], r[-1] + 1):
                minx, miny, maxx, maxy = args.boundingbox
                clean.remove_bbox(minx, miny, maxx, maxy, z)
        elif args.tilepath and args.AZ:
            for z in range(0, 21):
                tiles = clean.generate_tiles(args.tilepath, z)
                clean.remove_tiles(tiles, z)
        elif args.tilepath and args.zoom:
            r = list(set(args.zoom))
            r = sorted(r)
            for z in xrange(r[0], r[-1] + 1):
                tiles = clean.generate_tiles(args.tilepath, z)
                clean.remove_tiles(tiles, z)
