#!/usr/bin/env python

import multiprocessing
import socket
import struct
import fcntl
import commands
import os
import shutil
import ConfigParser
ctc_config = ConfigParser.RawConfigParser()

base_path = os.path.dirname(__file__)
ctchome = os.path.abspath(os.path.join(base_path, '../'))
NUMBER_OF_PROCESSES = multiprocessing.cpu_count()
wsgi_script_name = '../ctc.wsgi'
port = 8085
port2 = raw_input('\tEnter CTC port number (%s): ' % port)
if port2:
    print '\t CTC port number is %s' % port2
    port = port2
cache = raw_input('\tEnter your CTC cache directory root: ')
if not cache:
    print '\tNote: I am using the defualt directory:'
    cache = '/usr/local/ctc/cache'
    print '\t\t%s' % cache
else:
    if not os.access(cache, os.W_OK):
        print '\tNOTE: pass is not writable by user!'
        print '\tTry to do a "sudo chmod 777 -R %s"' % cache
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockfd = sock.fileno()
SIOCGIFADDR = 0x8915
import getpass
user = getpass.getuser()


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname), 'r').read()


def get_ip(iface='eth0'):
    '''Advanced method'''
    ifreq = struct.pack('16sH14s', iface, socket.AF_INET, '\x00' * 14)
    try:
        res = fcntl.ioctl(sockfd, SIOCGIFADDR, ifreq)
    except:
        return None
    ip = struct.unpack('16sH2x4s8x', res)[2]
    return socket.inet_ntoa(ip)


def get_ip2():
    '''Simple method'''
    ip = commands.getoutput("/sbin/ifconfig").split("\n")[1].split()[1][5:]
    return ip

ip = get_ip('eth0') or get_ip('eth1') or get_ip2()
ip2 = raw_input('\tEnter FQDN or IP (%s): ' % ip)
if ip2:
    print '\tNote: Your FQDN/IP is: %s' % ip2
    ip = ip2

version = read('../VERSION')


uwsgi_ini_base = read('uwsgi.conf.template')
app_ini_base = read('app.conf.template')
nginx_ctc_base = read('nginx.conf.template')
uwsgi_init = read('uwsgi.initd.template')
ctc_config_example = read('../config/ctc.ini.example')

web2py_xml = '''
<uwsgi>
    <socket>/tmp/web2py.socket</socket>
    <pythonpath>/home/farsheed/Documents/web2py</pythonpath>
    <module>wsgihandler:application</module>
</uwsgi>
'''

script = 'handlers:Main()'
server = '\tserver unix:///tmp/ctc.socket;\n'
servers = server
chdir = os.path.abspath(os.path.join(base_path, '../'))

data = uwsgi_ini_base.format(
    socket='/tmp/ctc.socket',
    chdir=chdir,
    p=NUMBER_OF_PROCESSES,
    version=version,
    home=ctchome,
    venv=os.path.abspath(os.path.join(base_path, '../../ctcdev')),
    script=script,
)
uwsgi_dir = os.path.join(base_path, 'uwsgi')
if not os.path.isdir(uwsgi_dir):
    os.mkdir(uwsgi_dir)
with open('%s/ctc.ini' % (uwsgi_dir), 'w') as f:
    f.write(data)


appdata = app_ini_base.format(
    chdir=chdir,
    version=version,
    home=ctchome,
    venv=os.path.abspath(os.path.join(base_path, '../../ctcdev')),
)
with open('%s/app.ini' % (uwsgi_dir), 'w') as f:
    f.write(appdata)

nginx_access_log = os.path.abspath(
    os.path.join(base_path, '../logs/nginx_access.log'))
nginx_error_log = os.path.abspath(
    os.path.join(base_path, '../logs/nginx_error.log'))
app_path = os.path.abspath(
    os.path.join(base_path, '../app'))
data = nginx_ctc_base % (
    servers, port, ip, nginx_access_log, nginx_error_log, app_path, cache)
nginx_dir = os.path.abspath(os.path.join(base_path, 'nginx'))
bin_dir = os.path.join(base_path)
if not os.path.isdir(nginx_dir):
    os.mkdir(nginx_dir)
with open('%s/ctc.conf' % (nginx_dir), 'w') as f:
    f.write(data)
daemon = os.path.abspath(os.path.join(base_path, '../../ctcdev/bin/uwsgi'))
data = uwsgi_init.replace('{{USER}}', user).replace('{{DAEMON}}', daemon).replace('{{CTCHOME}}', ctchome)
with open('uwsgi.initd', 'w') as x:
    x.write(data)


if not os.path.isfile('%s/config/ctc.ini' % ctchome):
    shutil.copyfile('%s/config/ctc.ini.example' % ctchome, '%s/config/ctc.ini' % ctchome)


print 'Generating Finished.'
